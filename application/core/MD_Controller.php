<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MD_Controller extends CI_Controller{ 
	
	function __construct(){  
 
		parent::__construct(); 		  
		
		date_default_timezone_set('Asia/Jakarta');
		/*$set=ini_set('mssql.textlimit','65536');
   		ini_set('mssql.textlimit',$set);

   		$set2=ini_set('mssql.textlimit','65536');
   		ini_set('mssql.textsize',$set2); 
		*/ 
   		

		$this->_initConfig();
		$this->output->enable_profiler(false);
		//$this->DATA = new globalModel();		

		//$this->domain = $_SERVER['SERVER_NAME'];
		//$this->write_log();

		//$this->user_online = $this->get_list_user();		
		//$this->load->library('database');
		
		$CI =& get_instance();
		$this->db2 = rtrim($CI->config->slash_item('db2name'),"/");  
		//$this->CI->load->database();
		//$this->db = $CI->load->database('default', TRUE);  
		//
		
	}
	
	/*protected function load_info()
	{
		$data = array(
			'keywords' => $this->AM->get_info('meta_keywords'),
			'description' => $this->AM->get_info('meta_description'),
			'webname' => $this->AM->get_info('website_name'),
			'tagline' => $this->AM->get_info('tagline'),
			'title' => $this->AM->get_info('title')
		);
		return $data;
	}*/
	
	/*function _vf($data=array()){
		/*$data["meta_desc"]		= $this->meta_desc;
		$data["meta_title"]		= $this->meta_title;
		$data["meta_keyword"] 	= $this->meta_keyword;
		$data["meta_image"] 	= $this->meta_image;
		
		$this->load->view('head',$data);
		$this->load->view('menu_left');		
		$this->load->view($data);	
	}*/
	function _initConfig(){
		$s = $this->session->userdata("jcfg");
		if(is_array($s))
			$this->jCfg = $s;
		else
			$this->_initSession();			
	}
	
	function isLogin(){
		if($this->jCfg['is_login'] == 1){
			redirect('');
		}else{
			redirect('auth');
		}
	}
	
	function current_session($i=0){
		for($x=0;$x>=$i;$x++){
			$this->_initSession();
		}
	}
	
	function _initSession(){
		$this->jCfg = array(
			'counter'		=> 0,
			'app_id'		=> '1',
			'app_name'		=> '',
			'is_live'		=> 0,
			'is_login'		=> 0,
			'view'			=> array("data"	=> "all","t" => "all"),
			'user' => array(
				'id' 		=> '',
				'name'		=> '',
				'fullname'	=> '',
				'level'	=> '',
				'is_all'	=> 0
			),
			'menu'			=> array(),
			'access'		=> array(),
			'lang'			=> 'en',
			'theme'		=> array(
				'name'		=> 'cloud',
				'dir'		=> 'cloud'
			)
		);
		$this->_releaseSession();
	}
	
	function _releaseSession(){
		$this->session->set_userdata(array("jcfg"=>$this->jCfg));
	}

	
	/*function setReferer($url=''){
		$this->jCfg['referer'] = $url;
		$this->_releaseSession();
	}
	
	function setMenu($menu=array()){
		$this->jCfg['menu'] = $menu;
		$this->_releaseSession();
	}
	
	function setMessage($message=''){
		$this->jCfg['message'] = $message;
		$this->_releaseSession();
	}
	
	
	function setLang($lang='eng'){
		$this->jCfg['lang'] = $lang;
		$this->_releaseSession();
	}
	function setAccess($acc=array()){
		$this->jCfg['access'] = $acc;
		$this->_releaseSession();
	}
	
	function getReferer(){
		return $this->jCfg['referer'];
	}
	*/
	function download($file, $name, $mime_type='')
	{		
			/*'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',*/
		//Check the file exist or not
		if(!is_readable($file)) die('File not found or inaccessible!');
		$size = filesize($file);
		$name = rawurldecode($name);

		/* MIME type check*/
		$known_mime_types=array(
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',            
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3'	=> 'audio/mpeg',			
			'mp4'	=> 'video/mp4',
            'qt' 	=> 'video/quicktime',
            'mov' 	=> 'video/quicktime',
			'mpa'	=> 'video/mpeg',
			'mpe'	=> 'video/mpeg',
			'mpeg'	=> 'video/mpeg',
			'mpg'	=> 'video/mpeg',

            // adobe
            'pdf' => 'application/pdf',
            //'psd' => 'image/vnd.adobe.photoshop',
			'psd' => 'image/x-photoshop',	
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
			
			'docx' =>  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'xlsx'	=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			'xltx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
			'potx' => 'application/vnd.openxmlformats-officedocument.presentationml.template',
			'ppsx' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
			'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
			'sldx' =>  'application/vnd.openxmlformats-officedocument.presentationml.slide',
			'dotx' =>  'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
			'xlam' => 'application/vnd.ms-excel.addin.macroEnabled.12',
			'xlsb' => 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',			

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet'			
		);

		if($mime_type=='')
		{
			$file_extension = strtolower(substr(strrchr($file,"."),1));
			if(array_key_exists($file_extension, $known_mime_types))
			{
				$mime_type=$known_mime_types[$file_extension];
			} 
			else 
			{
				$mime_type="application/force-download";
			};
		};

		//turn off output buffering to decrease cpu usage
		@ob_end_clean(); 

		// required for IE Only
		if(ini_get('zlib.output_compression'))
		ini_set('zlib.output_compression', 'Off'); 

		header('Content-Type: ' . $mime_type);
		header('Content-Disposition: attachment; filename="'.$name.'"');
		header("Content-Transfer-Encoding: binary");
		header('Accept-Ranges: bytes'); 
		/*non-cacheable */
		header("Cache-control: private");
		header('Pragma: private');
		header("Expires: Tue, 15 May 1984 12:00:00 GMT");

		// multipart-download and download resuming support
		if(isset($_SERVER['HTTP_RANGE']))
		{
			list($a, $range) = explode("=",$_SERVER['HTTP_RANGE'],2);
			list($range) = explode(",",$range,2);
			list($range, $range_end) = explode("-", $range);
			$range=intval($range);
			if(!$range_end) 
			{
				$range_end=$size-1;
			} 
			else 
			{
				$range_end=intval($range_end);
			}
			$new_length = $range_end-$range+1;
			header("HTTP/1.1 206 Partial Content");
			header("Content-Length: $new_length");
			header("Content-Range: bytes $range-$range_end/$size");
		} 
		else 
		{
			$new_length=$size;
			header("Content-Length: ".$size);
		}

		/* Will output the file itself */
		$chunksize = 1*(1024*1024); //you may want to change this
		$bytes_send = 0;
		if ($file = fopen($file, 'r'))
		{
			if(isset($_SERVER['HTTP_RANGE']))
			fseek($file, $range);
			while(!feof($file) && (!connection_aborted()) && ($bytes_send<$new_length))
			{
				$buffer = fread($file, $chunksize);
				print($buffer); //echo($buffer); // can also possible
				flush();
				$bytes_send += strlen($buffer);
			}
			fclose($file);
		} 
		else
			//If no permissiion
			die('Error - can not open file.');
			//die
			die();
	}
	/*
	* stuff function
	*/
	/*
	function _getClass(){ // return name of current class
		return $this->router->fetch_class();
	}
	
	function _getMethod(){ // return name of current methode
		return $this->router->fetch_method();
	}

	
	function sendEmail($p=array()){
		$this->load->library('email');
		
		/*mail method */
		/*$config['protocol'] = 'sendmail';
		$config['mailtype'] = isset($p['type'])?$p['type']:'html';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$config['priority'] = isset($p['priority'])?$p['priority']:3; */
		
		/*smtp method */
		/*$config['protocol']  = 'mail';
		$config['smtp_host'] = 'smtp.gmail.com';
		$config['smtp_port'] = 465;
		$config['smtp_user'] = 'hhumaedi@gmail.com';
		$config['smtp_pass'] = '*****';
		$config['priority']  = 1;
		$config['mailtype']  = 'html';
		$config['charset']   = 'utf-8';
		$config['wordwrap']  = TRUE;*/
		
		/* send mail method */ 
		/*$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';
		$config['charset']  = 'utf-8';
		$config['wordwrap'] = TRUE;
		
		$this->email->initialize($config);

		$this->email->from($p['from'], isset($p['title'])?$p['title']:'IAPI');
		$this->email->to($p['to']);
		if( isset($p['cc']) && trim($p['cc']) != "" ){			
			$this->email->cc($p['cc']); 
		}
		$this->email->subject('IAPI :'.$p['subject']);
		$this->email->message($p['message']);
		
		if(isset($p['alt_message']) && trim($p['alt_message'])!='' ){
			$this->email->set_alt_message($p['alt_message']);
		}
		
		$this->email->send();
		//debugCode($this->email->print_debugger());
		
	}

	function get_list_user(){
		$ses = $this->db->get("app_sessions")->result();
		$list_user = array();
		if( count($ses) > 0 ){
			foreach($ses as $r){
				$m = $r->user_data;
				$t = explode("username_chat_online",$m);
				$ntmp = isset($t[1])?explode("}",$t[1]):array();
				if( isset($ntmp[0]) ){
					$mx = str_replace(";","",$ntmp[0]);
					$expl = explode('"',trim($mx));
					if(isset( $expl[2] )){
						$list_user[$expl[2]] = 1;
					}
				} 
			}
		}
		
		return($list_user);
	}

	function _uploaded($par=array()){ 
		$this->load->library('image_lib');
		
		$uri = $this->upload_path;		
		$folder_upload = (isset($par['folder']))?$par['folder']:'original';
		if($_FILES[$par['input']]['error']==4)
			return false;
		$uId = uniqid();
		$fileName = $uId;
		$config['upload_path'] = $uri.$folder_upload."/";
		$config['file_name'] = $fileName;
		$config['allowed_types'] = $this->upload_allowed_types;
		$config['max_size']		= 1024*20;
		if(trim($this->upload_types)=='image'){
			$config['max_width']  	= 1024*5;
			$config['max_height'] 	= 768*5;
		}
		$this->load->library('upload');
		$this->upload->initialize($config);
		

		if( $this->upload->do_upload($par['input']) )
		{	
			//debugCode($config);
			$img = $uId.$this->upload->file_ext;
			
			$this->_delte_old_files($par['param']);
			$this->DATA->_update($par['param']['par'],array($par['param']['field']=>$img));
			
			if(trim($this->upload_types)=='image'){ 
			
				$fileNameResize = $config['upload_path'].$fileName.$this->upload->file_ext;
				$img = getimagesize($fileNameResize);
				$realWidth	= $img[0];
				$realHeight = $img[1];
				
				/* Watermarking */
				/*
				if($this->is_watermark == TRUE){
					$config = null;
					$config['source_image']	= $fileNameResize;
					$config['image_library'] = 'gd2';
					$config['wm_type'] = 'overlay';
					$config['wm_overlay_path'] = './assets/collections/wm_source.png';
					$config['wm_vrt_alignment'] = 'bottom';
					$config['wm_hor_alignment'] = 'right';
					$config['wm_opacity'] = '80';
					$this->image_lib->initialize($config); 
					$this->image_lib->watermark();
					$this->image_lib->clear();
					unset($config);
				}


				$resize = array();
				foreach($this->upload_resize as $r){
					$resize[] = array(
						"width"			=> $r['width'],
						"height"		=> $r['height'],
						"quality"		=> $r['quality'],
						"source_image"	=> $fileNameResize,
						"new_image"		=> $uri.$r['name']."/".$fileName.$this->upload->file_ext
					);
				}
				
				foreach($resize as $k=>$v){
					$oriW = $v['width'];
					$oriH = $v['height'];
					$x = $v['width']/$realWidth;
					$y = $v['height']/$realHeight;
					if($x < $y) {
						$v['width'] = round($realWidth*($v['height']/$realHeight));
					} else {
						$v['height'] = round($realHeight*($v['width']/$realWidth));
					}     
				
					$this->image_lib->initialize($v); 
					if(!$this->image_lib->resize()){
						debugCode($this->image_lib->display_errors());
						die("Error Resize....");
					}
					$this->image_lib->clear();
					if($k==0){
						$config = null;
						$config['image_library'] = 'GD2';
						$im = getimagesize($v['new_image']);
						$toCropLeft = ($im[0] - ($oriW *1))/2;
						$toCropTop = ($im[1] - ($oriH*1))/2;
						
						$config['source_image'] = $v['new_image'];
						$config['width'] = $oriW;
						$config['height'] = $oriH;
						$config['x_axis'] = $toCropLeft;
						$config['y_axis'] = $toCropTop;
						$config['maintain_ratio'] = false;
						
						$this->image_lib->initialize($config);
						 
						if(!$this->image_lib->crop()){
							die("Error Crop..");
						}
						$this->image_lib->clear();
					}
				}
			} // end if this type image
			
		}
		else {
			debugCode($this->upload->display_errors());
		}
	}
	
	/*
	function _delte_old_files($par=array()){
		$uri = $this->upload_path;
		$files = $this->DATA->data_id($par['par']);
		$folder = isset($par['folder'])?$par['folder'].'/':'original/';
		if( !empty( $files->{$par['field']} ) ){
			$ori_file = $uri.$folder.$files->{$par['field']};
			if(file_exists($ori_file)){
				unlink($ori_file);
			}
			if(trim($this->upload_types)=='image' && count($this->upload_resize) > 0){				
				$data = array();
				foreach($this->upload_resize as $m){
					$data[] = $uri.$m['name']."/".$files->{$par['field']};
				}	
				foreach($data as $v){
					if(file_exists($v)){
						unlink($v);
					}
				}
			}				
		}
	}	

	function _data_web($m=array()){
		
		$this->load->library('pagination');
		$config['per_page'] = $this->per_page;
		$data = $this->data_table;
		$config['base_url'] = $m['base_url'];
		$config['total_rows'] = $data['total'];		
		$config['uri_segment'] = $this->uri_segment;
		$config['suffix']	= $this->config->item('url_suffix');
		$config['full_tag_open'] = '<div class="pagination">';
		$config['full_tag_close'] = '</div>';	
		$config['next_link'] = '&rarr;';
		$config['prev_link'] = '&larr;';
		$this->pagination->initialize($config);
		
		return array(
			'data'			=>	$data['data'],			
			'cRec'			=>  $data['total'],
			'no'			=>  $this->uri->segment($this->uri_segment)==''?0:$this->uri->segment($this->uri_segment),
			'cPage'			=>  ceil($data['total']/$this->per_page),
			'paging'		=> 	$this->pagination->create_links()
		);		
	}	
	
	
	function _data($m=array()){
		
		$this->load->library('pagination');
		$config['per_page'] = $this->per_page;
		$data = $this->data_table;
		$config['base_url'] = $m['base_url'];
		$config['total_rows'] = $data['total'];		
		$config['uri_segment'] = $this->uri_segment;
		$config['suffix']	= $this->config->item('url_suffix');
		$config['full_tag_open'] = '<div class="pagination" align="right" style="margin-top:0px;"><ul>';
		$config['full_tag_close'] = '</ul></div>';	
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['next_link'] = '&rarr;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['prev_link'] = '&larr;';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		return array(
			'data'			=>	$data['data'],			
			'cRec'			=>  $data['total'],
			'no'			=>  $this->uri->segment($this->uri_segment)==''?0:$this->uri->segment($this->uri_segment),
			'cPage'			=>  ceil($data['total']/$this->per_page),
			'paging'		=> 	$this->pagination->create_links()
		);		
	}	

	function write_log(){
		
		$class 	= $this->_getClass();
		$method	= $this->_getMethod();
		$name	= $this->jCfg['user']['name'];
		$id 	= $this->jCfg['user']['id'];
		$ip		= $_SERVER['REMOTE_ADDR'];
		$browser= $_SERVER['HTTP_USER_AGENT'];
		$url 	= $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		
		$flag_ins = TRUE;
		if( (!isset($this->jCfg['current_class'])) && (!isset($this->jCfg['current_funtion'])) ){
			$this->jCfg['current_class'] = $class;
			$this->jCfg['current_funtion'] = $method;
			$this->_releaseSession();
		}else{
			
			if($this->jCfg['current_class']==$class && $this->jCfg['current_funtion']==$method){
				$flag_ins = FALSE;				
			}
			
			$this->jCfg['current_class'] = $class;
			$this->jCfg['current_funtion'] = $method;
			$this->_releaseSession();
		}
		
		if(!empty($id) && $flag_ins==TRUE && $this->jCfg['current_class']!="chat" ){
			
			$POST=isset($_POST)?json_encode($_POST):"";
			$GET=isset($_GET)?json_encode($_GET):"";
			$arr_method = array(
				"detail_member","detail_advertiser","cek_advertiser_username",
				"search","get_ads","detail_iklan","get_tag","report","get_tag",
				"get_tag_brand","get_reach","detail","get_ads_info","view_image",
				"get_city","test","index","im_lost","access"
			);
			if(!in_array($method,$arr_method)){
				$this->db->insert("app_log",array(
					"log_date"		=> date("Y-m-d H:i:s"),
					"log_class"		=> $class,
					"log_function" 	=> $method,
					"log_url"		=> $url,
					"log_user_id"	=> $id,
					"log_ip"		=> $ip,
					"log_role"		=> $this->jCfg['user']['level'],
					"log_user_agent"	=> $browser,
					"log_user_name"	=> $name,
					"log_var_get"	=> $GET,
					"log_var_post"	=> $POST			
				));
			}
		}
	}*/
	
}