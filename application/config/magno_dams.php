<?php
require "configuration.php";
$bUrl = $this->config['base_url'];
$config['image_url']	= $bUrl.'images/';
$config['asset_url']	= $bUrl.'assets/';
$config['theme_url']	= $bUrl.'theme/';
$config['file_url']	=  $bUrl.'files/';

$config['domain'] = $cfg['domain'];
$config['smtp_host'] = $cfg['smtp']['host'];
$config['smtp_username'] = $cfg['smtp']['username'];
$config['smtp_password'] = $cfg['smtp']['password'];

$config['db2name'] = $cfg['db2']['database'];

?>
