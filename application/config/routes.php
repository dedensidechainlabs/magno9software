<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/ 

$route['default_controller'] = "site/home";
//$route['404_override'] 		 = "auth/error404";
$route['login'] 			 = "auth/login";
$route['auth'] 			 = "auth";
//$route['home'] 				 = "site/home";
$route['home/(:num)'] 		 = "site/home/$1";
$route['home/(:num)/(:any)'] = "site/home/$1/$2";
$route['group'] 			 = "site/group";
$route['group/(:num)/(:any)']= "site/group/$1/$2";
$route['c/(:num)/(:any)'] = "site/c/$1/$2";

/*$route['file/(:num)/(:any)'] = "site/file/$1/$2";
$route['file/list/(:any)/(:any)'] = "site/file/list/$1/$2";

//$route['file/detail/(:any)'] = "site/file/$1/$2";
$route['file/detail/(:any)/(:any)'] = "site/file/detail/$1/$2";
//$route['file/detail/(:num)/(:any)'] = "site/file/detail/$1/$2";

$route['file/add'] 			= "site/file/add";
$route['file/edit/(:num)'] 	= "site/file/edit/$1";
$route['file/edit/(:any)/(:any)'] 	= "site/file/edit/$1/$2";
*/
$route['category/(:num)/(:any)'] = "site/category/$1/$2";
//$route['subcategory/(:num)/(:any)'] = "site/subcategory/$1/$2";
$route['subcategory/(:any)/(:any)'] = "site/subcategory/$1/$2";

$route['profile'] 				 = "user/profile";

$route['admin'] 				 = "admin";

$route['search'] 	= "site/search";
$route['readdata'] 	= "site/readdata";

/* End of file routes.php */
/* Location: ./application/config/routes.php */