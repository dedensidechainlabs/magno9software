<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
extends MD_Controller (not default CI_Controller)
file MD_Controller in path application/core

Controller & Function Modul File
- List
- Add
- Edit
- Detail
- Function get & load ajax fo file

Session: jCfg

@author      Agus Thoiba 
@copyright   Copyright (c) 2013 Digitall Division macs909.com
*/
class File extends MD_Controller { //Don't change this line
	 
	public function __construct()
	{
		parent::__construct();		
		$url = trim(str_replace(base_url(),"",current_url()));		
		if ($this->jCfg['is_login'] == 0) //check if user not login
		{
			redirect("auth?url=".$url);			
		}
		else
		{
			$this->$url = $url;
			$this->load->model('site_mod','S');
			$this->load->model('user_mod','U');
			if ($this->jCfg['user']['level']=='administrator')
			{
				$this->access = 0;
			}
		}
	}
	
	//file list
	public function p()
	{	
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2']= $this->uri->segment(2);
		$data['uri3'] = $this->uri->segment(3);
		$data['uri4'] = $this->uri->segment(4);
		if ($this->uri->segment(3)!='')
		{
			$sub_id = $this->uri->segment(3);
			$pfile_sub = array(
				'act'	=> 1,
				'sub_id'	=> $sub_id
			);		
			$qfile_sub = $this->S->get_file($pfile_sub)->result();		
			if (count($qfile_sub)==0)
			{
				$data['kosong'] = 'No record found';
				$data['content'] = 'file_list_v';
				$this->load->view('dashboard',$data);
			}
			else
			{
				$this->load->library('pagination');	
				if ($this->input->get('order')=='' OR $this->input->get('order')=='date')
				{
					$order_get = 'date';
					$by = 'file_created_date';
					$order = 'desc';
				}
				else if ($this->input->get('order')=='name')
				{
					$order_get = $this->input->get('order');
					$by = 'file_title';
					$order = 'asc';
				}	
				$config['base_url'] = site_url("file/p/$sub_id/?order=".$order_get.'&v='.$this->input->get('v'));
				//$config['enable_query_strings'] = TRUE;
				//$config['use_page_numbers'] = TRUE;
				$config['page_query_string'] = TRUE;
				$config['query_string_segment'] = 'page';
				$config['total_rows'] = count($qfile_sub);
				$config['per_page'] = 12;
			
				$this->pagination->initialize($config);	
				$data['links'] = $this->pagination->create_links();
				$data['total'] = count($qfile_sub);
				if ($this->input->get('page')=='')
					{$uripagi = 0;}
				else
					{$uripagi = $this->input->get('page');}		
		
				$pagi = array(
					'act' 	=> 1,
					'sub_id' => $sub_id,
					'limit_get' => $config['per_page'],
					'offset'=> $uripagi,
					'by'	=> $by, 
					'order'	=> $order
				);
				
				$qagi = $this->S->get_file($pagi)->result();		
				$data['query'] = $qagi;		
						
				$data['content'] = 'file_list_v';						
				$this->load->view('dashboard',$data);		
			}
		}
	}
	
	public function detail()
	{
		$id = $this->uri->segment(3);
		$data['q'] = $q = $this->S->get_file(array('id'=>$id))->row();
		if(count($q)==0)
		{
			$data['kosong'] = 'No Record Found';
			$data['content'] = 'no_record';	
		}
		else	
		{
			$data['url_title'] = url_title($q->file_title,"-",TRUE);
			if ($q->file_prev=='')	
			{
				$qex_type = $this->S->get_extension(array('ext'=>$q->file_extension))->row()->file_type; 							
				$qtype_img = $this->S->get_file_type(array('type'=>$qex_type))->row()->filetype_img;
				$data['med'] =  "<img src='".image_url()."$qtype_img'>";
			}
			else
			{						
				$prev_url = file_url()."preview/temp";				
				$ori_url = file_url()."original";
				if($q->file_extension == 'jpg' || $q->file_extension == 'png')
				{					
					$data['med'] = "<img src='$prev_url/$q->file_prev'>";
				}
				else if($q->file_extension == 'mp3') // if extension mp3 play with html5 audio
				{
					$data['med'] = "
						<audio controls>
							<source src='$ori_url/$q->file_prev' type='audio/mpeg'>
							Your browser does not support this audio format.
						</audio>"; 
				}
				else if($q->file_extension == 'mp4' || $q->file_extension == 'mpg') // if extension mp4 play with html5 video
				{
					$data['med'] = "
						<video width='auto' height='240' controls>
							<source src='$prev_url/$q->file_prev' type='video/mp4'>
							Your browser does not support the video tag.
						</video>"; 
				}
				else				
				{
					$data['med'] = "<img src='$prev_url/$q->file_prev'>";
				}
			}			
			
			$bladown = "
				<a href='".base_url()."file/download_start/$q->file_id' >
						<div class='btn'> 
							<img  width='14' height='14' src='".image_url()."download_icon2.png'> Download
						</div>
				</a>
			";
			// group access for download - BEGIN
			if($this->jCfg['user']['level']=='administrator') //check if admin all access 
			{
				$access_file = 0;
				$data['down'] = $bladown ;
			}
			else
			{
				$access_file = array(
					'user'	=> $this->jCfg['user']['id'],
					'group'	=> $q->file_group_id,
					'down'	=> 1
				);		
				$pcek = array(
					'id'	=>$id,
					'access'=>$access_file			
				);		
				$qcek = $this->S->get_file($pcek)->row(); // query allowed group download
				//print_r($qcek);
				// group access for download - END 		
				/*if ($q->is_private==1)			
				{
					$this->db->where('file_id', $q->file_id);
					$this->db->where('user_id',$this->jCfg['user']['id']);
					$qcek_private = $this->db->get('dam_file_access')->row();		
				}*/	
				
					if (count($qcek)==1) //check if allowed group download
					{
						if ($q->is_private==1) //check if file private
						{
							$this->db->where('file_id', $q->file_id);
							$this->db->where('user_id',$this->jCfg['user']['id']);
							$qcek_private = $this->db->get('dam_file_access')->row();		
							
							// check of created_by access to download & view
							$p_efaccess = array(
								'user_id' => $this->jCfg['user']['id'],
								'file_id' => $q->file_id
							);
							//echo $this->jCfg['user']['id'];
							$qefile_access = $this->S->edit_file_access($p_efaccess)->row();
							if(count($qefile_access)==1)
							{
								$data['down'] = $bladown ;
							}
							else
							{
								if ($qcek_private->view_down ==1 )
								{
									$data['down'] = $bladown ;
								}
								else if ($qcek_private->view_down ==0)
								{
									$data['down'] = '';
								}				
							}
						}
						else if($q->is_private==0)
						{
							$data['down'] = $bladown ;
						}
					}	
					else
					{
						$data['down'] = '';
					}	
				
			}
			// access for download - END		
			$data['content'] = 'file_detail';
		}
		$this->load->view('dashboard',$data);	
	}	
	
	public function add()
	{
		if (ceksize()==1)
		{
			$data['kosong'] = 'Disk Full';
			$data['content'] = 'no_record';	
		}
		else
		{
			$data['content'] = 'file_add';				
		}
		$this->load->view('dashboard',$data);
	}
		
	public function edit()
	{		
		$id = $this->uri->segment(3); //get file id with uri segment 3
		$qid = $this->S->get_file(array('id'=>$id))->row(); // select file by id 
		if( count($qid)==1) 
		{
			if($this->jCfg['user']['level']=='administrator') //check if admin all access 
				{$access_file = 0;}
			else
			{
				$access_file = array(
					'user'	=> $this->jCfg['user']['id'],
					'group'	=> $qid->file_group_id,
					'edit'	=> 1
				);
			}
			$p = array(
				'id'	=>$id,
				'access'=>$access_file			
			);
			$q = $this->S->get_file($p)->row();		
			if(count($q)==1)
			{
				//|| ($q->is_private == 0)
				// check file private
				//echo $q->dam_file.created_by; 
				$p_efaccess = array(
					'user_id' => $this->jCfg['user']['id'],
					'file_id' => $q->file_id
				);
				//echo $this->jCfg['user']['id'];
				$qefile_access = $this->S->edit_file_access($p_efaccess)->row();
				//echo "<pre>";print_r($qefile_access);echo "</pre>";
				//echo count($qefile_access);
				if ( ($this->jCfg['user']['level']=='administrator') || ($q->is_private==1 && count($qefile_access)==1) || ($q->is_private==0) )
				//if ( ($this->jCfg['user']['level']=='administrator') || ($q->is_private == 1 && count($qfile_access>1)) )
				{												
						$data['file_id'] = $q->file_id ;
						$data['file_title'] = $q->file_title ;
						$data['file_code'] = get_code($q->file_id) ;
						$data['file_desc'] = $q->file_desc ;
						$data['file_keyword'] = $q->file_keyword ;
						$data['file_creator'] = $q->file_creator ;
						$data['file_created_date'] = $q->file_created_date;
						$data['file_thumbnail'] = $q->file_thumbnail ;
						$data['file_prev'] = $q->file_prev ;
						
						$path_thumbnail = "files/thumbnail/temp/$q->file_thumbnail";
						$img_thumbnail =  base_url().$path_thumbnail;
						if ($q->file_thumbnail == '' )
						{
							$data['file_thumbnail_cont'] = "
								
								<iframe style='height:90px;' id='frame-thumbnail' src='".base_url()."site/upload_thumbnail' ></iframe>
								<input type='hidden' name='file_thumbnail' id='name-thumbnail' value='' />";
						}
						else
						{								
							
							if($q->file_extension == 'jpg' || $q->file_extension == 'png')
							{
								
								$data['file_thumbnail_cont'] = "
									<div id='thumbnail_cont'><img src='$img_thumbnail'></div>
									<input type='hidden' name='file_thumbnail' id='name-thumbnail' value='$q->file_thumbnail' />
								";
								$data['file_thumbnail_cont'] = "
								<div id='thumbnail_cont'><img src='$img_thumbnail' width='50px'></div>
								<iframe style='height:90px;' id='frame-thumbnail' src='".base_url()."site/upload_thumbnail' ></iframe>
								<input type='hidden' name='file_thumbnail' id='name-thumbnail' value='' />";
								
							}
							else
							{
								$data['file_thumbnail_cont'] = "
									<div id='thumbnail_cont'><img  src='$img_thumbnail'></div>
									<input type='hidden' name='file_thumbnail' id='name-thumbnail' value='$q->file_thumbnail' />
								";
								$data['file_thumbnail_cont'] = "
								<iframe style='height:90px;' id='frame-thumbnail' src='".base_url()."site/upload_thumbnail' ></iframe>
								<input type='hidden' name='file_thumbnail' id='name-thumbnail' value='' />";
							}
						
						}
						
						
						
						$path_prev = "files/preview/thumbnail/$q->file_prev";
						
						if ($q->file_prev == '' )
						{
							$data['file_prev_cont'] = "
								<iframe style='height:90px;' id='frame-prev' src='".base_url()."site/upload_prev' ></iframe>
								<input type='hidden' name='file_prev' id='name-prev' value='' />";
						}
						else
						{								
							$img_prev =  base_url().$path_prev;
							$prev_url = file_url()."preview/temp";
							$ori_url = file_url()."original";
							if($q->file_extension == 'jpg' || $q->file_extension == 'png')
							{
								
								$data['file_prev_cont'] = "
									<div id='prev_cont'><img src='$img_prev'></div>
									<input type='hidden' name='file_prev' id='name-prev' value='$q->file_prev' />
								";
								
								
							}
							else if($q->file_extension == 'mp3') // if extension mp3 play with html5 audio
							{
								$data['file_prev_cont'] = "
									<audio controls>
										<source src='$ori_url/$q->file_prev' type='audio/mpeg'>
										Your browser does not support this audio format.
									</audio>
									<input type='hidden' name='file_prev' id='name-prev' value='$q->file_prev' />
								"; 
							}
							else if($q->file_extension == 'mp4') // if extension mp4 play with html5 video
							{
								if(substr($q->file_prev,-3)=='mp4'){
									$data['file_prev_cont'] = "
										<input type='hidden' name='file_prev' id='name-prev' value='$q->file_prev' />
										<video width='auto' height='100' controls>
										<source src='$prev_url/$q->file_prev' type='video/mp4'>
											Your browser does not support the video tag.
										</video>
										<iframe style='height:90px;' id='frame-prev' src='".base_url()."site/upload_prev' ></iframe>
						
									";
								}else{
									$data['file_prev_cont'] = "
										<input type='hidden' name='file_prev' id='name-prev' value='$q->file_prev' />
										<div id='prev_cont'><img  src='$img_prev'></div>
										<iframe style='height:90px;' id='frame-prev' src='".base_url()."site/upload_prev' ></iframe>
						
									";	
								}											
							}
							else
							{
								$data['file_prev_cont'] = "
									<div id='prev_cont'><img  src='$img_prev'></div>
									<input type='hidden' name='file_prev' id='name-prev' value='$q->file_prev' />
								";	
							}
						
						}
						
						$data['file_ori'] = $q->file_ori ;
						$data['file_extension'] = $q->file_extension ;
						$data['is_private'] = $q->is_private ;
						if ($q->is_private == 1)
						{
							$data['check'] = 'checked';
							$pb = array('file_id'=> $q->file_id);
							$qb = $this->S->get_file_access($pb)->result();		
							$ret = array();	
							$data['user_private'] = '';
							$n = 0;
							foreach($qb as $row)
							{
								$n++;
								$full_name = $this->U->get(array('id'=>$row->user_id))->row()->user_fullname;
								array_push($ret,array('name'=>$full_name,'id'=>$row->user_id));
								$data['user_private'] .= $row->user_id;
								if ($n < count($qb))
								{
										$data['user_private'] .= ',';
								}
							}   
							$data['pre_populate'] = (json_encode($ret));
							
							
						}
						else
						{
							$data['check'] = '';
							$data['pre_populate']= '[]'; 
						}
						
						$data['img_prev'] = file_url().'preview/thumbnail/'.$q->file_prev;
						if ($q->file_prev=='' OR file_exists("files/preview/thumbnail/$q->file_prev")==0)	
							{$data['text'] = "<a id='prev_klik' href='#'>upload new file </a>";}
						else
							{$data['text'] = "<a id='prev_klik' href='#'>remove & upload new file </a>";}
						$data['content'] = 'file_edit';		
					
				}
				else
				{
					$data['kosong'] = 'Not Allowed';
					$data['content'] = 'no_record';	
				}
			}
			else
			{
				$data['kosong'] = 'Not Allowed';
				$data['content'] = 'no_record';	
			}
		}
		else
		{
			$data['kosong'] = 'Not Record Found';
			$data['content'] = 'no_record';	
		}			
		$this->load->view('dashboard',$data);			
	}	

	public function add_edit_post()
	{		
		if ($this->input->post('submit')!='')
		{			
			if (ceksize()==1)
			{
				$data['kosong'] = 'Disk Full';
				$data['content'] = 'no_record';	
			}
			else
			{
				$group_id = $this->input->post('group_id');
				$cat_id = $this->input->post('cat_id');
				$sub_id = $this->input->post('sub_id');
				$file_title = $this->input->post('file_title');
				$file_desc = $this->input->post('file_desc');
				$file_keyword = $this->input->post('file_keyword');
				$file_created_date = $this->input->post('file_created_date');
				$file_creator = $this->input->post('file_creator');
				$file_thumbnail = $this->input->post('file_thumbnail');
				$file_prev = $this->input->post('file_prev');
				$file_ori = $this->input->post('file_ori');
				$file_extension = $this->input->post('file_extension');
				//$active = $this->input->post('active');
				$is_private = $this->input->post('is_private');			
				$user_private = $this->input->post('user_private');			
			
				if ($this->uri->segment(3)=='add')
				{				
					//$qidlast = $this->db->query("SELECT max(file_id) as file_id_max FROM dam_file")->result();
					//$idlast = $qidlast->file_id_max;
					//$file_id = $idlast+1;
					$data = array(
						
						'file_group_id' => $group_id,
						'file_cat_id' => $cat_id,
						'file_sub_id' => $sub_id,
						'file_title' => $file_title,
						'file_desc' => $file_desc,
						'file_keyword' => $file_keyword,
						'file_creator' => $file_creator,
						'file_thumbnail' => $file_thumbnail,
						'file_prev' => $file_prev,
						'file_ori' => $file_ori,
						'file_extension' => $file_extension,
						'file_created_date' => $file_created_date,
						'is_private' => $is_private,
						'active'	=> 1,			
						'created_date'	=> date("Y-m-d H:i:s"),
						'created_by'	=> $this->jCfg['user']['id'],
						'created_ip'	=> $this->input->ip_address()
					);
					$q = $this->db->insert('dam_file',$data);//insert to dam_file
					if ($q)
					{
						//echo "insert file success";
						//get last id: limit=1 
						$q_file = $this->S->get_file(array('limit' => 1 ))->row();	
						$rec_file_id = $q_file->file_id;					
						if ($is_private==1) //is private = 1 
						{
							$this->private_file_ins($user_private,$rec_file_id);//insert private file function 						
						}					
						$rec_file_title = url_title($q_file->file_title,"-",TRUE);					
						redirect('file/detail/'.$rec_file_id.'/'.$rec_file_title);
					}
					else
					{echo "insert file failed";}
				}
				else if ($this->uri->segment(3)=='edit')
				{
					$data = array(
						'file_title' => $file_title,
						'file_desc' => $file_desc,
						'file_keyword' => $file_keyword,
						'file_created_date' => $file_created_date,
						'file_creator' => $file_creator,
						'file_thumbnail' => $file_thumbnail,
						'file_prev' => $file_prev,
						'file_ori' => $file_ori,
						'file_extension' => $file_extension,
						'is_private' => $is_private,
						//'active'	=> 1,			
						'modified_by'	=> $this->jCfg['user']['id'],
						'modified_ip'	=> $this->input->ip_address()
					);
					$file_id = $this->input->post('file_id');
					$this->db->where('file_id', $file_id);
					$qupd = $this->db->update('dam_file',$data);	
					if ($qupd)
					{
						//echo "insert file success";
						//get last id: limit=1 
						$q_file = $this->S->get_file(array('id' => $file_id ))->row();				
						$rec_file_id = $q_file->file_id;
						if ($is_private==1) //is private = 1 
						{
							$this->private_file_ins($user_private,$rec_file_id);//insert private file function 						
						}
						$rec_file_title = url_title($q_file->file_title,"-",TRUE);
						redirect("file/detail/$rec_file_id/$rec_file_title");
					}
					else
					{echo "update file failed";}				
				}
			}
		}
		else
		{
			redirect("file/add","refresh");
		}
	}
	
	public function private_file_ins($p_user,$p_file)
	{		
		$struser = explode(",",$p_user);
		foreach($struser as $bla)
		{
			$p = array(
				'user_id'	=> $bla,
				'file_id'	=> $p_file
			);
			$qcek = $this->S->check_file_access($p)->row();			
			if (count($qcek)==0)
			{				
				$dat = array(
					'user_id'	=> $bla,
					'file_id'	=> $p_file,
					'view_down'	=> 1,
					'created_date' => date("Y-m-d H:i:s"),
					'created_by' => $this->jCfg['user']['id'],
					'created_ip' => $this->input->ip_address()
				);
				$ins = $this->db->insert('dam_file_access',$dat);
				if (!$ins){die("insert record error");}
			}
			else
			{
				$dat = array(
					'view_down'	=> 1,
					'modified_by' => $this->jCfg['user']['id'],
					'modified_ip' => $this->input->ip_address()
				);
				$this->db->where('user_id',$bla);
				$this->db->where('file_id',$p_file);
				$upd = $this->db->update('dam_file_access',$dat);
				if (!$upd){die("update record error");}			
			}							
		}
		return TRUE;
	}
	
	public function get_file_aj($id)
	{
		$q = $this->S->get_file(array('id'=>$id))->row();
		return $q->file_name;
	}	
	
	//delete record in table dam_file
	public function delete() 
	{		
		$q = $this->input->get('q');
		//echo $q;
		//$s = explode("#",$q);		echo $s[1];
		
		$file_id = $q;
		
		$qid = $this->S->get_file(array('id'=>$file_id))->row(); //get id file	for
		if(count($qid)==0)
		{
			echo "file not found";
		}
		else
		{
			if($this->jCfg['user']['level']=='administrator') //check if admin all access 
				{$access_file = 0;}
			else
			{
				$access_file = array(
					'user'	=> $this->jCfg['user']['id'],
					'group'	=> $qid->file_group_id,
					'del'	=> 1
				);
			}
			$pcek = array(
				'id'	=>$file_id,
				'access'=>$access_file			
			);
			
			//Query check id for access delete group		
			$qcek = $this->S->get_file($pcek)->row();
			if (count($qcek)==1)
			{
				$delete_file_upload = $this->unlink($file_id);			
				/*$p = array(
					'value'		=> $file_id,
					'pk'		=> $s[2],
					'table_name'=> $s[3]
				);*/
				$p_del = array(
					'file_id'	=> $file_id,
				);
				$this->db->where('file_id',$file_id);
				$del = $this->db->delete('dam_file');
				
				if ($del &&  $delete_file_upload)
					{echo "delete record success";}
				else
					{echo "delete record failed";}		
			}
			else
			{
				echo "not allowed";
			}
		}
	}
	
	//delete file ori & prev 
	public function unlink($id) 
	{
		$rec = $this->S->get_file(array('id'=>$id))->row();
		$path = 'files/';
		if ($rec->file_ori != '' && file_exists($path.'original/'.$rec->file_ori)==1)
		{
			unlink($path.'original/'.$rec->file_ori);//delete file in thumbnail
		}
		if ($rec->file_prev != '')
		{
			if ($rec->file_ori != '' && file_exists($path.'preview/temp/'.$rec->file_prev)==1)
			{
				unlink($path.'preview/temp/'.$rec->file_prev);//delete file in preview temp
			}
			if ($rec->file_ori != '' && file_exists($path.'preview/thumbnail/'.$rec->file_prev)==1)
			{
				unlink($path.'preview/thumbnail/'.$rec->file_prev);//delete file in preview thumbnail
			}
		}
		return TRUE;
	}
	
	/* 
	Download original file
	using download library in core/MD_Controller
	*/
	function download_start($id)
	{
		$id = $this->uri->segment(3);
		$qid = $this->S->get_file(array('id'=>$id))->row(); //get id file
		if($this->jCfg['user']['level']=='administrator') //check if admin all access 
			{$access_file = 0;}
		else
		{
			$access_file = array(
				'user'	=> $this->jCfg['user']['id'],
				'group'	=> $qid->file_group_id,
				'down'	=> 1
			);
		}
		$pcek = array(
			'id'	=>$id,
			'access'=>$access_file			
		);
		$qcek = $this->S->get_file($pcek)->row(); //check if allowed
		if (count($qcek)==1)
		{
			$filename =  $qcek->file_ori;
			set_time_limit(0);
			$file_path = 'files/original/'.$filename ;
			$this->download($file_path, ''.$filename.'');
			return TRUE;
		}	
	}
	
	public function export_to_excel()
	{
		$data['group_id'] = $group_id = $this->input->get('group_id');
		$data['media'] = $media = $this->input->get('media');
		$data['year'] = $year = $this->input->get('year');
		$data['find'] = $find = $this->input->get('find');
		$p = array(			
			'act'		=> 1,
			'group_id'	=> $group_id,
			'year'		=> $year,
			'type'		=> $media,
			'find'		=> $find,
			//'is_private'=> $is_private,
			//'private'	=> $private				
		);
		$token = generate_token(5);
		$data['query'] = $this->S->get_file($p)->result();
		
		$this->output->set_header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
		$this->output->set_header("Content-type:   application/x-msexcel; charset=utf-8");
		$this->output->set_header("Content-Disposition: attachment; filename=magno9-$token.xls"); 
		$this->output->set_header("Expires: 0");
		$this->output->set_header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		$this->output->set_header("Cache-Control: private",false); 		
		$this->load->view('export_excel_v',$data);
	}
	
}