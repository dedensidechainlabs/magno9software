<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
extends MD_Controller (not default CI_Controller)
file MD_Controller in path application/core

Controller & Function Modul File
- Report

Session: jCfg
@author      Agus Thoiba 
@copyright   Copyright (c) 2013 Digitall Division macs909.com
*/
class Report extends MD_Controller { //Don't change this line
	 
	public function __construct()
	{
		parent::__construct();		
		$url = trim(str_replace(base_url(),"",current_url()));		
		if ($this->jCfg['is_login'] == 0) //check if user not login
		{
			redirect("auth?url=".$url);			
		}
		else
		{
			$this->$url = $url;
			$this->load->model('site_mod','S');
			$this->load->model('user_mod','U');
			if ($this->jCfg['user']['level']=='administrator')
			{
				$this->access = 0;
			}
		}
	}
	
	public function index()
	{
		$data['content'] = 'report_v';
		$this->load->view('dashboard',$data);
	}
	
	public function excel()
	{
		$data['group_id'] = $group_id = $this->input->get('group_id');
		if($this->jCfg['user']['level']=='administrator') //check if admin all access 
			{$access_file = 0;}
		else
		{
			$access_file = array(
				'user'	=> $this->jCfg['user']['id'],
				'group'	=> $group_id,
				'view'	=> 1
			);
		}
		
		if($group_id!='')
		{
			$data['group_name'] = $this->S->get_group(array('id'=>$group_id))->row()->group_name;
		}
		else
		{
			$data['group_name'] = 'All';
		}		
		$data['cat_id'] = $cat_id = $this->input->get('cat_id');
		if($cat_id!='')
		{
			$data['cat_name'] = $this->S->get_category(array('id'=>$cat_id))->row()->cat_name;
		}
		else
		{
			$data['cat_name'] = 'All';
		}
		$data['sub_id'] = $sub_id = $this->input->get('sub_id');
		if($sub_id!='')
		{
			$data['sub_name'] = $this->S->get_subcategory(array('id'=>$sub_id))->row()->sub_name;
		}
		else
		{
			$data['sub_name'] = 'All';
		}
		$year = $this->input->get('year');
		if($year!='')
			{$data['year'] = $year;}
		else
			{$data['year'] = 'All';}
		
		$p = array(			
			'act'		=> 1,
			'group_id'	=> $group_id,
			'cat_id'	=> $cat_id,
			'sub_id'	=> $sub_id,
			'year'		=> $year,			
			'access'	=> $access_file				
		);
		$token = generate_token(5);
		$date = date("Ymd");
		$q = $this->S->get_file($p)->result();
		if(count($q)==0)
		{
			$data['empty'] = 'No record found';
			$data['content'] = 'report_v';
			$this->load->view('dashboard',$data);
		}
		else
		{
			$data['query'] = $q;		
			$this->output->set_header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
			$this->output->set_header("Content-type:   application/x-msexcel; charset=utf-8");
			$this->output->set_header("Content-Disposition: attachment; filename=magno9-$date-$token.xls"); 
			$this->output->set_header("Expires: 0");
			$this->output->set_header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			$this->output->set_header("Cache-Control: private",false); 
			$this->load->view('export_excel_v',$data);
		}		
	}
}