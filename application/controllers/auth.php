<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends MD_Controller
{
	public function __construct()
	{
		parent::__construct();			
		$this->load->model('user_mod','U');				
		$this->load->model('master_mod','M');	
		if (!$this->input->get('url'))
			{$this->url= '';}
		else
			{$this->url= '/?url='.$this->input->get('url');}		
		
	}
	
	
	function index()
	{
		//echo "<pre>";print_r($this->session->userdata);echo "</pre>";
		//$this->session->sess_destroy();
		$init = get_Initial();
		if (count($init)==0)
		{
			redirect("auth/setup");
			//$this->setup();
			//$this->url= '';
		}
		else
		{
			$data['url'] = $this->url;
			$data['user_name'] = "";
			$data['error'] = "";
			$this->load->view('login_v',$data);
		}
	}
	
	function setup()
	{
		$this->load->view('setup_v');
	}
	
	function setup_post()
	{
		if (isset($_POST['submit']) && $_POST['submit'])
		{					
			$master_key = $this->input->post('master_key');
			if($master_key=='')
			{redirect("auth/setup");}
			$client_name = trim($_POST['client_name']);
			$user_name = trim($_POST['user_name']);
			$user_fullname = trim($_POST['user_fullname']);
			//echo $master_key;			
			$row = $this->M->get(array('master_key'=>$master_key))->row();
			if (count($row) == 0)
			{
				echo 'master_key anda tidak terdaftar<br/>';
				echo "<a href='".base_url()."auth/setup'>Back</a>";
			}
			else
			{
				//echo 'master_key cocok';
				$end_contract = $row->end_contract;
				$max_storage  = $row->pack_max_storage;
				$total_user     = $row->pack_max_user; 
				$domain		  = $row->domain;		
				$password = sha1($_POST['password']);						
				$date = date("Y-m-d H:i:s");
				
				$data_initial = array(
					'id'			=> 1,
					'master_key'	=> $master_key,
					'client_name'	=> $client_name,
					'end_contract'	=> $end_contract,
					'max_storage'	=> $max_storage,
					'total_user'	=> $total_user,
					'created_date'	=> $date,
					'created_ip'	=> $this->input->ip_address()
				);				
				$ins = $this->db->insert('dam_initial',$data_initial);
			
				$data_admin = array(
					'user_name'		=> $user_name,
					'password'		=> $password,
					'user_fullname'	=> $user_fullname,
					'level'			=> 'administrator',
					'active'		=> 1,
					'created_date'	=> $date,
					'created_ip'	=> $this->input->ip_address()
				);		
				$ins_admin = $this->db->insert('dam_user',$data_admin);
				if ($ins==TRUE AND $ins_admin==TRUE)
				{
					echo '<h1>installation success</h1>';
					//echo '<button id="btn">Click to installation Complete</button>';
					echo "<a href='".site_url()."'>Begin</a>";
				}
				else
				{
					echo 'error';
					echo "<a href='".base_url()."auth/setup'>Try again!</a>";
				}				
			}
		}	
	}

	function login()
	{
		$user_name = trim($this->input->post('user_name',TRUE));
		$password =$this->input->post('password',TRUE);	
		
		$p = array(
			'user_name'	=> $user_name,
			'password'  => $password
		);
		$rec = $this->U->cek($p)->row();
		if(count($rec)==1)
		{
			$this->jCfg['is_login'] = 1;
			$this->jCfg['user']['id'] = $rec->user_id;
			$this->jCfg['user']['name'] = $rec->user_name;
			$this->jCfg['user']['level'] = $rec->level;
			$this->jCfg['user']['fullname'] = $rec->user_fullname;
			$this->_releaseSession();
			//echo "ok";
			//echo "<pre>";print_r($this->session->userdata);echo "</pre>";
			redirect($this->input->get('url'),'refresh');
		}
		else
		{
			$data['url'] = $this->url ;
			$data['user_name'] = $user_name;
			$data['error'] = "The username or password you entered is incorrect";			
			$this->load->view('login_v',$data);
		}
	}
	
	function logout()
	{		
		$this->session->sess_destroy();
		redirect(site_url("auth"));
	}	
	
	function error404()
	{
		echo '404 Error !!!';
	}
}
?>