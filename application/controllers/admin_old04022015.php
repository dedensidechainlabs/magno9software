<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
    }
	
	public function index()
	{
		$data['title'] = 'Magno9 | Digital Asset Management System';
		$this->load->view('vadminlogin',$data);
	}	
	
	public function login()
	{
		$txtusername =  $this->input->post('txtusername');
		$txtpassword =  $this->input->post('txtpassword');
		$qcekusername = $this->db->query("SELECT * FROM TUSER WHERE USEREMAIL='$txtusername' AND USERPASSWORD='$txtpassword' AND USERLEVEL=1");
		$valcekusername = $qcekusername->num_rows();
		if($valcekusername == 1){
			$row = $qcekusername->row();
			$userlevel = $row->USERLEVEL;
			$this->load->library('session');
			$this->session->set_userdata('username', $txtusername);
			$this->session->set_userdata('userlevel', $userlevel);
			redirect('admin/dashboard','refresh');
		}else{
			
			echo '<script>alert("Wrong Username or Password! Please Try Again!")</script>';
			echo "<script>window.history.back();</script>";
			//redirect('admin','refresh');
		}
	}
	
	public function dashboard()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Magno9 | Digital Asset Management System';
			$this->load->model('madmin');
			$data['totalnewsletter'] = count($this->madmin->gettallnewsletter());
			$data['totalmoreinfo'] = count($this->madmin->gettallmoreinfo());
			$data['totalfreetrial'] = count($this->madmin->gettallfreetrial());
			$data['totalrequestpresentation'] = count($this->madmin->gettallrequestpresentation());
			$data['totalrequestquote'] = count($this->madmin->gettallrequestquote());
			$data['totalaffiliate'] = count($this->madmin->gettallaffiliate());
			$data['totaluser'] = count($this->madmin->gettalluser());
			$this->load->view('vadmindashboard',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	public function newsletter()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Magno9 | Digital Asset Management System';
			$this->load->model('madmin');
			$data['gettallnewsletter'] = $this->madmin->gettallnewsletter();
			$this->load->view('vadminnewsletter',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	public function moreinfo()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Magno9 | Digital Asset Management System';
			$this->load->model('madmin');
			$data['gettallmoreinfo'] = $this->madmin->gettallmoreinfo();
			$this->load->view('vadminmoreinfo',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	public function freetrial()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Magno9 | Digital Asset Management System';
			$this->load->model('madmin');
			$data['gettallfreetrial'] = $this->madmin->gettallfreetrial();
			$this->load->view('vadminfreetrial',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	public function requestpresentation()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Magno9 | Digital Asset Management System';
			$this->load->model('madmin');
			$data['gettallrequestpresentation'] = $this->madmin->gettallrequestpresentation();
			$this->load->view('vadminrequestpresentation',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	public function requestquote()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){$data['title'] = 'Magno9 | Digital Asset Management System';
			$this->load->model('madmin');
			$data['gettallrequestquote'] = $this->madmin->gettallrequestquote();
			$this->load->view('vadminrequestquote',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	public function affiliate()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){$data['title'] = 'Magno9 | Digital Asset Management System';
			$this->load->model('madmin');
			$data['gettallaffiliate'] = $this->madmin->gettallaffiliate();
			$this->load->view('vadminaffiliate',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	public function affiliatedetail()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){$data['title'] = 'Magno9 | Digital Asset Management System';
			$data['uri1'] = $uri1 = $this->uri->segment(1);
			$data['uri2'] = $uri2 = $this->uri->segment(2);
			$data['uri3'] = $uri3 = $this->uri->segment(3);
			$data['uri4'] = $uri4 = $this->uri->segment(4);
			$this->load->model('madmin');
			$data['getallaffiliatedetail'] = $this->madmin->getallaffiliatedetail($uri3);
			$this->load->view('vadminaffiliatedetail',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	public function user()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){$data['title'] = 'Magno9 | Digital Asset Management System';
			$this->load->model('madmin');
			$data['gettalluser'] = $this->madmin->gettalluser();
			$this->load->view('vadminuser',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function logout()
	{
		$this->session->sess_destroy();
		redirect('admin');
	}
}