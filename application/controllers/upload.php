<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
extends MD_Controller (not default CI_Controller)
file MD_Controller in path application/core

Controller & Function Module Upload bulk
using plupload library http://www.plupload.com/

Session: jCfg


@author & @copyright   2013 Digitall Division macs909.com
*/
class Upload extends MD_Controller { //Don't change this line
	public function __construct()
	{
		parent::__construct();			
		if ($this->jCfg['is_login'] == 0) //check if user not login
		{
			$url = trim(str_replace(base_url(),"",current_url()));
			redirect("auth?url=".$url);			
		}
		else
		{
			$this->load->model('site_mod','S');
		}
	}
	
	public function index()
	{
		if (ceksize()==1)
		{
			$data['kosong'] = 'Disk Full';
			$data['content'] = 'no_record';	
		}
		else
		{
			$data['content'] = 'upload_v';
		}
		$this->load->view('dashboard',$data);		
	}
	
	public function bulk()
	{
		if (ceksize()==1)
		{
			$data['kosong'] = 'Disk Full';
			$data['content'] = 'no_record';	
		}
		else
		{
			$data['content'] = 'upload_bulk_v';
		}
		$this->load->view('dashboard',$data);	
	}
	
	public function ext_allow() //allowed upload files extension
	{			
		$q = $this->S->get_extension()->result();			
		$n = 0;
		echo '"';
		foreach ($q as $row)
		{
			$n++;
			echo $row->file_ext_name;
			if (count($q)!=$n)
				{echo ",";}
		}
		echo '"';
	}
	
	public function post()
	{
		//if($this->input->post('submit'))
		//{
		$count = 0; 
		if ($_POST['uploader_count'] == 0)
		{
			redirect ('upload/bulk');
		}
		else
		{
			if (ceksize()==1)
			{
				$data['kosong'] = 'Disk Full';
				$data['content'] = 'no_record';	
			}
			else
			{
				//echo "<pre>";print_r($_POST);echo "</pre>";				
				$n= 0;
				$sub_id = $_POST['sub_id'];
				$group_name = $this->S->get_group(array('id'=>$_POST['group_id']))->row()->group_prefix;
				$data['sub_name'] = $this->S->get_subcategory(array('id'=>$sub_id))->row()->sub_name;
				$data['file_name'] = '';
				$data['msg'] = '';
				//echo $sub_id;
				foreach ( $_POST as $name => $value )  
				{				
					//echo htmlentities(stripslashes($name));$file_ori = nl2br(htmlentities(stripslashes($value)));$n % 2 != 0 	
					$n++;
					if ( $n % 2 == 0 AND $n > 3	 AND is_numeric($value)==0)
					{
						//echo "<pre>"; print_r($value); echo "</pre>";
						$str_file_ori = explode(".",$value);
						$file_extension = strtolower($str_file_ori[count($str_file_ori)-1]);
					
						if ($file_extension == 'jpg' || $file_extension == 'png')
						{	
							chmod("files/original/$value", 0755);
							copy("files/original/$value","files/preview/temp/$value");						
							$this->create_thumb("files/original/$value","files/preview/thumbnail/$value");							
							$fprev_name = $value;
							//echo $value;
						}
						else if ($file_extension == 'mp3' || $file_extension == 'mp4')
						{
							$fprev_name = $value;
							//copy("files/original/$value","files/preview/temp/$value");
						}
						else
						{
							$fprev_name = '';
						}					
						$q = $this->insert_id($value,$fprev_name,$file_extension,$sub_id,$group_name,$_POST['group_id'],$_POST['cat_id']);
						$data['file_name'] .= $value;
						$data['file_name'] .= '<br>';
						if ($q) 
							{$data['msg'] .= "<img src='".image_url()."success.png'>";}
						else
							{$data['msg'] .= "<img src='".image_url()."cross.png'>";}
						$data['msg'] .= '<br>';
					}					
				}				
				$data['content'] = 'upload_bulk_post_v';
				$this->load->view('dashboard',$data);
			}
		}
		/*}
		else
		{
			redirect('upload/bulk');
		}*/
		//echo "<script>alert('Insert Success')</script>";
		//redirect('upload');
	}
	
	function create_thumb($filesrc,$dest)
	{		
		$this->load->library('image_moo'); 
		$q = $this->image_moo->load($filesrc)->resize_crop(120,120)->save($dest,true);
		if($q)
		{
			return $q;
		}
		else if($this->image_moo->errors)
		{
			print $this->image_moo->display_errors();
		}
	}
	
	function create_thumbtest()
	{		
		$this->load->library('image_moo'); 
		$filesrc = "files/preview/temp/1.JPG";
		$dest = "files/preview/thumbnail/1.JPG";
		
		//$thumb = $path.'thumbnail/'.$file_name;
		$this->image_moo->load($filesrc)->resize_crop(120,120)->save($dest,true);		
		if($this->image_moo->errors)
		{
			print_r ($this->image_moo->display_errors());
		}
	}
	
	public function insert_id($fname,$fnameprev,$fext,$fsub,$fgroup,$group_id,$cat_id)
	{
		$date = date("Y-m-d H:i:s");
		$dat = array(	
			'file_group_id'		=> $group_id,
			'file_cat_id'		=> $cat_id,
			'file_sub_id'		=> $fsub,
			'file_prev'			=> $fnameprev,
			'file_ori'			=> $fname,
			'file_title'		=> $fname,
			'file_extension'	=> $fext,
			'active'			=> 1,
			'file_creator' 		=> $this->jCfg['user']['fullname'],
			'file_created_date' => $date,
			'created_date'		=> $date,
			'created_by'		=> $this->jCfg['user']['id'],
			'created_ip'		=> $this->input->ip_address()
		);
		$q = $this->db->insert('dam_file',$dat);
		if (!$q)
			{die('insert db error');}
		else
			return $q;
	}
	
	public function proccess()
	{
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Pragma: no-cache");

		/* 
		// Support CORS
		header("Access-Control-Allow-Origin: *");
		// other CORS headers if any...
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
			exit; // finish preflight CORS requests here
		}
		*/

		// 5 minutes execution time
		@set_time_limit(5 * 60);
		// Uncomment this one to fake upload time
		// usleep(5000);
		// Settings
		//$targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";
		$targetDir = 'files/original';
		$cleanupTargetDir = true; // Remove old files
		$maxFileAge = 5 * 3600; // Temp file age in seconds


		// Create target dir
		if (!file_exists($targetDir)) {
			@mkdir($targetDir);
		}

		// Get a file name
		if (isset($_REQUEST["name"])) 
			{$fileName = $_REQUEST["name"];} 
		elseif (!empty($_FILES)) 
			{$fileName = $_FILES["file"]["name"];} 
		else 
			{$fileName = uniqid("file_");}

		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

		// Chunking might be enabled
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

		// Remove old temp files	
		if ($cleanupTargetDir) 
		{
			if (!is_dir($targetDir) || !$dir = opendir($targetDir)) 
			{
				die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
			}

			while (($file = readdir($dir)) !== false) 
			{
				$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
				// If temp file is current file proceed to the next
				if ($tmpfilePath == "{$filePath}.part") 
				{
					continue;
				}
				// Remove temp file if it is older than the max age and is not the current file
				if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) 
				{
					@unlink($tmpfilePath);
				}
			}
			closedir($dir);
		}	

		// Open temp file
		if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) 
		{
			die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}

		if (!empty($_FILES)) 
		{
			if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) 
			{
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
			}
			// Read binary input stream and append it to temp file
			if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) 
			{
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
			}
		} 
		else 
		{	
			if (!$in = @fopen("php://input", "rb")) 
			{
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
			}	
		}

		while ($buff = fread($in, 4096)) 
		{
			fwrite($out, $buff);
		}

		@fclose($out);
		@fclose($in);

		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) 
		{
			// Strip the temp .part suffix off 
			rename("{$filePath}.part", $filePath);
		}

		// Return Success JSON-RPC response
		die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
	}
}