<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
extends MD_Controller (not default CI_Controller)
file MD_Controller in path application/core

Controller & Function
- Home
- Group
- Category
- Sub Category
- Function get & load ajax

Session: jCfg

@author      Digitall Division macs909.com
@copyright   Copyright (c) 2013 macs909.com
*/

class Site extends MD_Controller { //Don't change this line
	 
	public function __construct()
	{
		
		parent::__construct();		
		$url = trim(str_replace(base_url(),"",current_url()));		
		if ($this->jCfg['is_login'] == 0) //check if user not login
		{			
			redirect("auth?url=".$url);	
			
		}
		else
		{
			$this->load->model('site_mod','S');
			if ($this->jCfg['user']['level']=='administrator')
			{
				$this->access = 0;
			}
		}
		
	}
	
	public function index()
	{
		$this->home;
	}
	
	public function popup() 
	{	
		$alert = $this->input->get('alert');
		if (isset($alert) && $alert!='')
		{
			$data['kosong'] = 'No record found';
			$data['content'] = 'file_list_v';
			$this->load->view('dashboard',$data);
		}
		else
		{
			return false;
		}
	}	
	function readdata()
	{
		$data['title'] = 'MAGNO9';
		$this->load->view('readdata',$data);	
	}
	public function tes_popup()
	{
		$data['alert'] = 'No record found';
		$this->load->view('alert',$data);
	}
	public function home()
	{
		$data['uri2'] = $uri2 = $this->uri->segment(2);
		$data['uri3'] = $uri3 = $this->uri->segment(3);
		$data['uri4'] = $uri4 = $this->uri->segment(4);
		$data['uri5'] = $uri5 = $this->uri->segment(5);
		$data['uri6'] = $uri6 = $this->uri->segment(6);
		$data['uri7'] = $uri7 = $this->uri->segment(7);
		if ($uri2 == '')// group
		{		
			if ($this->jCfg['user']['level']=='administrator')
			{
				$access = 0;
			}
			else
			{
				$access = array(
					'user'	=> $this->jCfg['user']['id'],
					'view'	=> 1
				);
			}
			$p_group = array(
				'act'	=> 1,
				'order' => 'asc',
				'by'	=> 'group_name',
				'access' => $access 
			);		
			$q = $this->S->get_group($p_group)->result();	
			if (count($q)==0)
			{
				$data['kosong'] = 'No Record Found';
				$data['content'] = 'no_record';				
			}
			else
			{	
				$data['q'] = $q;
				$data['content'] = 'group_v';				
			}			
			$this->load->view('dashboard',$data);	
		}
	}
	
	public function category()
	{	
		$uri2 = $this->uri->segment(2);
		$data['uri3'] = $uri3 = $this->uri->segment(3);
		$p = array(
				'act' => 1,
				'group_id'	=> $uri2,
				'by'  => 'cat_name',
				'order'=> 'asc'
		);
		$q = $this->S->get_category($p)->result();
		if (count($q)==0)
		{
			$data['kosong'] = 'No Record Found';
			$data['content'] = 'no_record';				
		}
		else
		{
			$data['q'] = $q;			
			$data['content'] = 'category_v';	
		}		
		$this->load->view('dashboard',$data);
	}
	
	public function subcategory()
	{	
		$cat_id = $this->uri->segment(2);
		$p = array(
				'act' 	=> 1,
				'cat_id'=> $cat_id,
				'by'  	=> 'sub_name',
				'order'	=> 'asc'
		);
		$q = $this->S->get_subcategory($p)->result();
		if (count($q)==0)
		{
			$data['kosong'] = 'No Record Found';
			$data['content'] = 'no_record';						
		}
		else
		{
			$data['q'] = $q;			
			$data['content'] = 'subcategory_v';
		}		
		$this->load->view('dashboard',$data);	
	}
	
	/*
	 - Group list 
	 - Group detail
	 - Category list by group selected
	*/
	

	public function c()
	{	
		$uri2 = $this->uri->segment(2);
		$uri3 = $this->uri->segment(3);
		$data['title'] = ucwords(str_replace("_"," ",$uri3));
		$p = array(
			'act'		=> 1,
			'group_id'	=> $uri2
		);			
		$data['q'] = $q = $this->S->get_category($p)->result();			
		$data['content'] = 'category_v';
		$this->load->view('dashboard',$data);		
	}
	
	
	public function search()
	{		
		$data['uri1'] = $uri1 = $this->uri->segment(1);
		$data['uri2'] = $uri2 = $this->uri->segment(2);
		$data['uri3'] = $uri3 = $this->uri->segment(3);
		$data['uri4'] = $uri4 = $this->uri->segment(4);
		$data['group_id'] =  $group_id = $this->input->get('group_id');
		$data['media'] = $media = $this->input->get('media');
		$data['year'] = $year = $this->input->get('year');
		$data['find'] = $find = $this->input->get('find');
		$data['page'] = $page = $this->input->get('page');
		if($this->jCfg['user']['level']=='administrator')
		{
			$access_file = 0;
		}
		else
		{	
			$access_file = array(
				'user'	=> $this->jCfg['user']['id'],
				'group'	=> $group_id,
				'view'	=> 1
			);
		}
		$p = array(			
			'act'		=> 1,
			'group_id'	=> $group_id,
			'year'		=> $year,
			'type'		=> $media,
			'find'		=> $find,
			'access'	=> $access_file				
		);
		$q = $this->S->get_file($p)->result();
		//echo "<pre>";print_r($q);echo "</pre>";
		if (count($q)==0)
		{
			$data['error'] = "No record found";		
		}
		else
		{		
			$this->load->library('pagination');	
			if ($this->input->get('order')=='' OR $this->input->get('order')=='date')
			{
				$order_get = 'date';
				$by = 'file_created_date';
				$order = 'desc';
			}
			else if ($this->input->get('order')=='name')
			{
				$order_get = $this->input->get('order');
				$by = 'file_title';
				$order = 'asc';
			}		
			$config['base_url'] = site_url("search?group_id=$group_id&media=$media&year=$year&order=$order_get&v=".$this->input->get('v'));						
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'page';
			$config['total_rows'] = count($q);
			$config['per_page'] = 12;
			
			$this->pagination->initialize($config);	
			$data['links'] = $this->pagination->create_links();
			$data['total'] = count($q);
			if ($this->input->get('page')=='')
				{$uripagi = 0;}
			else
				{$uripagi = $this->input->get('page');}		
			$pagi = array(
				'act' 	=> 1,
				'limit_get' => $config['per_page'],
				'offset'=> $uripagi,
				'by'	=> $by, 
				'order'	=> $order,
				'group_id'	=> $group_id,
				'year'		=> $year,
				'type'		=> $media,
				'find'		=> $find,
				'access'	=> $access_file					
			);
			$qagi = $this->S->get_file($pagi)->result();		
			//echo "<pre>";print_r($qagi);echo "</pre>";
			$data['query'] = $qagi;			
		}		
		$data['content'] = 'file_list_v';
		$this->load->view('dashboard',$data);			
	}
	
	
	public function cari()
	{
		$p = array(
			'act'		=> 1 //,'findname'	=> $find				
		);
		$data['q'] = $this->S->get_file($p)->result();		
		$this->load->view('cari',$data);		
	}
	
	//get group for option add
	public function get_group()
	{
		$group_id = $this->input->get('group_id');
		$add = $this->input->get('add');
		$view = $this->input->get('view');
		$par = array('add'=>1);
		$qbla = get_group($par); 	
		if($this->jCfg['user']['level']=='administrator')
		{
			//$gr_access = 0;	
			$q = $this->S->get_group($p)->result();			
		}
		else
		{
			$q = $qbla;
		}
		
		echo "<option value=''>-Please Select-</option>";
		foreach($q as $row)
		{
			if ($group_id == $row->group_id)
			{
				echo "<option value='$row->group_id' selected>$row->group_name</option>";
			}
			else
			{
				echo "<option value='$row->group_id'>$row->group_name</option>";
			}			
		}				
	}
	//get category by group selected for option 
	public function get_cat()
	{
		$gr = $this->input->get('gr');		
		
		if ($gr=='')
		{
			echo "<option value=''>-Select Group First-</option>";
		}
		else
		{			
			$p = array(
				'act' 		=> 1,
				'group_id' 	=> $gr,
				'order' 	=> 'asc',
				'by'		=> 'cat_name'		
			);
			$q = $this->S->get_category($p)->result();
			if (count($q)==0)
			{
				echo "<option value=''>-Empty-</option>";
			}
			else
			{
				echo "<option value=''>-Select Category-</option>";
				foreach($q as $row)
				{
					echo "<option value='$row->cat_id'>$row->cat_name</option>";				
				}		
			}
		}
		
	}
	
	//get subcategory by category selected for option 
	public function get_sub()
	{
		$gr = $this->input->get('gr');	
		$cat = $this->input->get('cat');
		if ($gr=='' && $cat=='')
		{
			echo "<option value=''>-Select Category First-</option>";
		}
		else
		{
			$p = array(
				'act' 		=> 1,
				'cat_id' 	=> $cat,
				'order' 	=> 'asc',
				'by'		=> 'sub_name'		
			);
			$q = $this->S->get_subcategory($p)->result();
			if (count($q)==0)
			{
				echo "<option value=''>-Empty-</option>";
			}
			else
			{
				echo "<option value=''>-Select Sub Category-</option>";
				foreach($q as $row)
				{
					echo "<option value='$row->sub_id'>$row->sub_name</option>";
				}		
			}
		}
	}
	
	/* START Upload Preview */
	
	// form Upload Preview
	// path = 'files/preview/'
	
	public function upload_prev()
	{		
		$url = $this->input->get('url'); //if upload again get filename
		$path = 'files/preview/'; //path download
		if ($url !='')
		{
			//unlink($path.'thumbnail/'.$url);//delete file in thumbnail
			unlink($path.'temp/'.$url);//delete file in temp
		}
		$data['error'] = '';
		$this->load->view('upload_prev_v',$data);
	}
	public function upload_thumbnail()
	{		
		$url = $this->input->get('url'); //if upload again get filename
		$path = 'files/thumbnail/'; //path download
		if ($url !='')
		{
			//unlink($path.'thumbnail/'.$url);//delete file in thumbnail
			unlink($path.'temp/'.$url);//delete file in temp
		}
		$data['error'] = '';
		$this->load->view('upload_thumbnail_v',$data);
	}
	
	function upload_thumbnail_post()
	{
		//$type = $this->input->get('type');
		
		$ext=preg_replace("/.*\.([^.]+)$/","\\1", $_FILES['userfile']['name']);
		$thumbnailname= $_FILES['userfile']['name'];
		
			if ( $ext=='jpg' OR $ext=='png' OR $ext=='mp4' OR $ext=='mp3')
			{
				$config['upload_path'] = './files/thumbnail/temp/';
				$config['max_size'] = 50*1024;
				$config['allowed_types'] = '*';
				//$config['encrypt_name'] = TRUE;
				$this->load->library('upload', $config);		
		
				$up = $this->upload->do_upload();
				if (!$up)
				{
					$error = array('error' => $this->upload->display_errors());
					$this->load->view('upload_thumbnail_v',$error);
				}
				else
				{
					$data['upload_data'] = $upload_data = $this->upload->data();
					$data['path'] = $path = 'files/thumbnail/';
					
					$fname_asli = $upload_data['file_name'];
					/*$token = substr(generate_token(32),0,6);
					$fname_b	= $token.'_'.str_replace(" ","-",$fname_asli);
					rename($path.'temp/'.$fname_asli, $path.'temp/'.$fname_b); */
					$data['file_name'] = $file_name = $fname_asli;
					
					$file = $path.'temp/'.$file_name;
					$thumb = $path.'thumbnail/'.$file_name;
					$this->create_thumb($file,$thumb); 
					$this->load->view('upload_thumbnailimg_post',$data);
				}			
			}
			else
			{
				$data['error'] = 'File must be JPG or PNG. max 20MB';
				$this->load->view('upload_thumbnail_v',$data);
			}	
	}
	
	
	function upload_prev_post()
	{
		//$type = $this->input->get('type');
		$ext=preg_replace("/.*\.([^.]+)$/","\\1", $_FILES['userfile']['name']);
		
			if ( $ext=='jpg' OR $ext=='png' OR $ext=='mp4' OR $ext=='mp3')
			{
				$config['upload_path'] = './files/preview/temp/';
				$config['max_size'] = 100*1024;
				$config['allowed_types'] = '*';
				//$config['encrypt_name'] = TRUE;
				$this->load->library('upload', $config);		
		
				$up = $this->upload->do_upload();
				var_dump($ext);
				if (!$up)
				{
					$error = array('error' => $this->upload->display_errors());
					$this->load->view('upload_prev_v',$error);
				}
				else
				{
					$data['upload_data'] = $upload_data = $this->upload->data();
					$data['path'] = $path = 'files/preview/';
					
					$fname_asli = $upload_data['file_name'];
					/*$token = substr(generate_token(32),0,6);
					$fname_b	= $token.'_'.str_replace(" ","-",$fname_asli);
					rename($path.'temp/'.$fname_asli, $path.'temp/'.$fname_b); */
					$data['file_name'] = $file_name = $fname_asli;
					
					$file = $path.'temp/'.$file_name;
					$thumb = $path.'thumbnail/'.$file_name;
					$this->create_thumb($file,$thumb); 
					$this->load->view('upload_previmg_post',$data);
				}			
			}
			else
			{
				$data['error'] = 'File must be JPG or PNG. max 20MB';
				var_dump($ext);
				$this->load->view('upload_prev_v',$data);
			}	
	}
	
	function create_thumb($filesrc=null,$dest=null)
	{		
		$this->load->library('image_moo'); 
		//$thumb = $path.'thumbnail/'.$file_name;
		$this->image_moo
			->load($filesrc)
			->resize_crop(120,120)
			->save($dest,true);
		return TRUE;
	}
	
	/* END Upload Preview */
	
	
	/* START Upload Ori */
	
	// form Upload Original
	// path = 'files/preview/'
	public function upload_ori()
	{		
		$url = $this->input->get('url'); //if upload again get filename
		$path = 'files/original/'; //path download
		if ($url !='')
		{
			unlink($path.$url);//delete file
		}
		$data['error'] = '';
		$this->load->view('upload_ori_v',$data);
	}
	
	function upload_ori_post()
	{
		//$type = $this->input->get('type');
		$ext=preg_replace("/.*\.([^.]+)$/","\\1", $_FILES['userfile']['name']);
		//cek extension allowed in table file_extension
		$cek_ext = extension_allowed($ext);
		
			if ( count($cek_ext)==1)
			{
				$config['upload_path'] = './files/original/';
				$config['max_size'] = 50000*1024;
				$config['allowed_types'] = '*';
				//$config['encrypt_name'] = TRUE;
				$this->load->library('upload', $config);		
		
				$up = $this->upload->do_upload();
				if (!$up)
				{
					$error = array('error' => $this->upload->display_errors());
					$this->load->view('upload_prev_v',$error);
				}
				else
				{
					$data['upload_data'] = $upload_data = $this->upload->data();
					$data['path'] = $path = 'files/original/';
					$fname_asli = $upload_data['file_name'];
					/*$token = substr(generate_token(32),0,6);
					$fname_b	= $token.'_'.str_replace(" ","-",$fname_asli);
					rename($path.$fname_asli, $path.$fname_b); */
					$data['file_name'] = $file_name = $fname_asli;
					$this->load->view('upload_ori_post',$data);
				}			
		}
		else
		{
				$data['error'] = "Empty/Extension not allowed/File's size > 500MB";
				$this->load->view('upload_ori_v',$data);
		}
	}
	public function set_code()
	{	
		$group_id = $this->input->get('group_id');		
		$id = $this->S->get_file(array('limit'=>1))->row()->file_id;
		$new_id = $id+1;
		if ($group_id!='')
		{
			$code = $this->S->get_group(array('id'=>$group_id))->row()->group_prefix;
			$date = date("Ymd");		
			echo $code."-".$date."-".$new_id;
		}
		else
		{
			echo '';
		}
	}
	/* END Upload Ori */
	
	
	
	public function tahun()
	{
		$q = $this->S->get_file_yearlist()->result();		
		//print_r($q);
		foreach ($q as $row)
		{
			echo $row->thn;
			//echo "<option value='$row->tahun'>$row->tahun</option>";
		}
		
	}
}

/* End of file site.php */
/* Location: ./application/controllers/site.php */