<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
extends MD_Controller (not default CI_Controller)
file MD_Controller in path application/core
Back End 
Controller & Function
- Home
- Group
- Category
- File
- Function get & load ajax

Session: jCfg

@author      Agus Thoiba 
@copyright   Copyright (c) 2013 Digitall Division macs909.com
*/
class Admin extends MD_Controller { //Don't change this line
	 
	public function __construct()
	{
		parent::__construct();			
		if ($this->jCfg['is_login'] == 0 )
		{
			redirect("auth");
		}		
		else
		{			
			//$this->pass = substr(generate_token(32),0,6);
			if ($this->jCfg['user']['level']!='administrator')
			{
				$data['alert'] = 'Not Allowed';
				$this->load->view('alert_js',$data);				
			}
			else
			{
				$this->load->model('site_mod','S');
				$this->load->library('grocery_CRUD');
			}
		}
	}
	
	function index()
	{	
		$data['output'] = "";
		$data['css_files'] = "";
		$data['js_files'] = "";
		$data['content'] = 'admin_v';
		$this->load->view('dashboard',$data);
	}
	
	/* Grocery CRUD library
		http://www.grocerycrud.com
	*/
	public function _example_output($output = null)
		{$this->load->view('dashboard.php',$output);}
	public function offices()
		{$output = $this->grocery_crud->render();$this->_example_output($output);}
	public function p()
		{$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));}
	
	// callback for date field default format yyyy-mm-dd
	public function _cb_date($value, $row) 
	{
		$dj = $value;
		if (substr($dj,0,10)== date("Y-m-d"))
			{return date("H:i",strtotime($dj)); }
		else
			{return date("d M y",strtotime($dj)); }		 	
	}
	
	// callback for status field i.e active/publish value 1=active, 0=inactive
	public function _cb_active($value, $row) 
	{
		if ($value==0)
			{return "<center><img src='".image_url()."cross.png'></center>";}
		else
			{return "<center><img src='".image_url()."success.png'></center>";}		 
	}
	
	public function  _cb_photo($value, $row) 
	{
		if ($value=='')
			{return "<center><img  src='".file_url()."user/photo40.jpg'></center>";}
		else
			{return "<center><img width='40' height='50' src='".file_url()."user/$value'></center>";}		 
	}
	
	public function _cb_thumb($value,$row) 
	{
		if ($value > 0 )
		{
			$img = $this->S->get_icon(array('id'=>$value))->row()->icon_image;
			return "<center><img width='40' height='40' src='".file_url()."icon/$img'></center>";
		}
		else
		{
			return FALSE;
		}
	}
	
	// callback before upload GC
	function _cb_before_upload_icon($files_to_upload,$field_info)
	{
		/*
		* Examples of what the $files_to_upload and $field_info will be:    
		$files_to_upload = Array
		(
			[sd1e6fec1] => Array
			(
                [name] => 86.jpg
                [type] => image/jpeg
                [tmp_name] => C:\wamp\tmp\phpFC42.tmp
                [error] => 0
                [size] => 258177
			) 
		) 
		$field_info = stdClass Object
		(
			[field_name] => file_url
			[upload_path] => assets/uploads/files
			[encrypted_field_name] => sd1e6fec1
		) 
		   
		if(is_dir($field_info->upload_path))
			{return true;}
		else
			{return 'I am sorry but it seems that the folder that you are trying to upload doesn\'t exist.';  }
		*/
		$type = $files_to_upload[$field_info->encrypted_field_name]['type'];
		$size = $files_to_upload[$field_info->encrypted_field_name]['size'];
		if ( $size <=  2*1024*1024 AND ($type == 'image/jpeg' OR $type == 'image/png'))
			{return true;}
		else
			{return 'JPG,PNG only & size least then 2MB';} 
	}
	
	function _cb_after_upload_icon($uploader_response,$field_info, $files_to_upload)
	{
		$this->load->library('image_moo'); 
		//Is only one file uploaded so it ok to use it with $uploader_response[0].		
		$file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 
		//print_r($this->image_moo->get_data_stream($filename=$file_uploaded));
		//$ori = $field_info->upload_path.'/image_original/'.$uploader_response[0]->name;
		$arr_folder = explode("/",$field_info->upload_path);
		$thumb = 'files/icon/'.$uploader_response[0]->name;
		$pic = $this->image_moo->load($file_uploaded);
		$pic->resize_crop(120,120)->save($thumb,true);
		return true;		
	}
	
	
	function _cb_before_upload_photo($files_to_upload,$field_info)
	{
		$type = $files_to_upload[$field_info->encrypted_field_name]['type'];
		$size = $files_to_upload[$field_info->encrypted_field_name]['size'];
		if ( $size <=  2*1024*1024 AND $type == 'image/jpeg')
			{return true;}
		else
			{return 'JPG only & size least then 2MB';} 
	}	
		
	function _cb_after_upload_photo($uploader_response,$field_info, $files_to_upload)
	{
		$this->load->library('image_moo'); 
		$file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 
		$pic = $this->image_moo->load($file_uploaded);
		$pic->resize_crop(200,250)->save("files/user/".$uploader_response[0]->name,true);
		return true;		
	}
	
	public function company_name()
	{
		$crud = new grocery_CRUD();
		//$crud->set_theme('datatables');
		$crud->set_table('dam_initial');
		$crud->display_as('client_name','Company Name');		
		$crud->unset_add();	$crud->unset_list();$crud->unset_back_to_list();$crud->unset_delete();$crud->unset_export();
		$crud->edit_fields('client_name','modified_date','modified_by','modified_ip');	
		// log record edit
		$crud->field_type('modified_by', 'hidden', $this->jCfg['user']['id']);		
		$crud->field_type('modified_date', 'hidden', date('Y-m-d H:i:s'));
		$crud->field_type('modified_ip', 'hidden', $this->input->ip_address());
		
		$output = $crud->render();
		$this->_example_output($output);
	}
	
	public function prefix()
	{
		try{
			$crud = new grocery_CRUD();
			//$crud->set_theme('datatables');
			$crud->set_table('dam_group');
			$crud->set_subject('Prefix');
			$crud->unset_delete();$crud->unset_add();$crud->unset_read();
			$crud->display_as('group_prefix','Prefix Asset Code');
			$crud->display_as('group_name','Group');
			$crud->columns('group_name','group_prefix');
			$crud->edit_fields('group_name','group_prefix');
			$crud->required_fields('group_prefix');
			$crud->field_type('group_name', 'readonly');	
			$output = $crud->render();
			$this->_example_output($output);
			
			}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function icon()
	{
		try{
			$crud = new grocery_CRUD();
			//$crud->set_theme('datatables');
			$crud->set_table('dam_icon');
			$crud->set_subject('Icon');
			$crud->unset_read();$crud->unset_export();
			
			$crud->columns('icon_name','icon_image','icon_active');
			$crud->callback_column('icon_active',array($this,'_cb_active'));
			$crud->unset_add_fields('modified_date','modified_by','modified_ip');			
			$crud->unset_edit_fields('created_date','created_by','created_ip');	
			$crud->required_fields('icon_name','icon_image');			
			$crud->set_field_upload('icon_image','files/icon');
			$crud->callback_before_upload(array($this,'_cb_before_upload_icon'));
			$crud->callback_after_upload(array($this,'_cb_after_upload_photo'));
			$crud->field_type('created_by', 'hidden', $this->jCfg['user']['id']);		
			$crud->field_type('created_date', 'hidden', date('Y-m-d H:i:s'));
			$crud->field_type('created_ip', 'hidden', $this->input->ip_address());		
			// log record edit
			$crud->field_type('modified_by', 'hidden', $this->jCfg['user']['id']);		
			$crud->field_type('modified_date', 'hidden', date('Y-m-d H:i:s'));
			$crud->field_type('modified_ip', 'hidden', $this->input->ip_address());
			$output = $crud->render();
			$this->_example_output($output);
			
			}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function cb_field_group_icon($value = 1)
	{			
		//return '+30 <input type="text" maxlength="50" value="'.$value.'" name="group_icon_id" style="width:462px">';		 
		$this->load->helper('form');
		$p = array(
			'sel'	=> 1,
			'act'	=> 1 
		);
		$options = $this->S->get_icon($p)->result();
		foreach($options as $row)
		{
			$row->icon_id;
		}
		//$options = array('' => 'Please select', 'Beige' => 'Beige' , 'Black' => 'Black', 'Blue' => 'Blue');
		//echo form_dropdown('colour', $options, $value,"id=col");
		//echo "<pre>";print_r($options);echo "</pre>";
	}	
	
	function cb_field_icon($value = '', $primary_key = null)
	{	
		$p = array(
			'act' => 1			
		);	
		$q = $this->S->get_icon($p)->result();		
		$img = '';
		foreach($q as $row)
		{
			if ($value==$row->icon_id)
				{$cek = 'checked';}
			else
				{$cek = '';}
			$img .= "
				<input type='radio' name='group_icon_id' value='$row->icon_id' $cek> 
				<img width='64' height='64' src='".file_url()."icon/$row->icon_image'>			
			";
		}
		return $img;		
	}
	
	public function group()
	{
		//try{
			$crud = new grocery_CRUD();
			//$crud->set_theme('datatables');
			$crud->set_table('dam_group');
			$crud->callback_field('group_icon_id',array($this,'cb_field_icon'));		
			$crud->callback_column('group_icon_id',array($this,'_cb_thumb'));
			$crud->set_subject('Group');
			$crud->display_as('group_icon_id','Icon');
			
			$crud->columns('group_name','group_prefix','group_icon_id','active','created_date');
			
			$crud->callback_column('created_date',array($this,'_cb_date'));
			$crud->callback_column('active',array($this,'_cb_active'));
			$crud->required_fields('group_name','group_prefix');
		
			$crud->unset_add_fields('modified_date','modified_by','modified_ip');			
			$crud->unset_edit_fields('created_date','created_by','created_ip');	
			$crud->unset_read();$crud->unset_export();
			$crud->unset_texteditor('group_desc');
			
			/*$crud->callback_before_upload(array($this,'_cb_before_upload_group'));
			$crud->callback_after_upload(array($this,'_cb_after_upload'));*/
			// log record add
			$crud->field_type('created_by', 'hidden', $this->jCfg['user']['id']);		
			$crud->field_type('created_date', 'hidden', date('Y-m-d H:i:s'));
			$crud->field_type('created_ip', 'hidden', $this->input->ip_address());		
			// log record edit
			$crud->field_type('modified_by', 'hidden', $this->jCfg['user']['id']);		
			$crud->field_type('modified_date', 'hidden', date('Y-m-d H:i:s'));
			$crud->field_type('modified_ip', 'hidden', $this->input->ip_address());
			
			$output = $crud->render();
			$this->_example_output($output);
			
		/*}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}*/
	}
	
	public function category()
	{
		try{
			$crud = new grocery_CRUD();
			//$crud->set_theme('datatables');
			$crud->set_table('dam_category');
			$crud->set_relation('cat_group_id','dam_group','group_name');
			$crud->set_subject('Category');
			$crud->columns('cat_name','cat_group_id','cat_icon_id','active','created_date');
			$crud->callback_field('cat_icon_id',array($this,'cb_field_icon'));		
			$crud->callback_column('cat_icon_id',array($this,'_cb_thumb'));
			$crud->callback_column('created_date',array($this,'_cb_date'));
			$crud->callback_column('active',array($this,'_cb_active'));
			$crud->display_as('cat_icon_id','Icon');
			$crud->display_as('cat_group_id','Group');
			$crud->display_as('cat_name','Name');
			$crud->display_as('cat_desc','Description');
			$crud->required_fields('cat_name','cat_group_id');
			$crud->unset_add_fields('cat_icon','modified_date','modified_by','modified_ip');			
			$crud->unset_edit_fields('cat_icon','created_date','created_by','created_ip');	
			$crud->unset_read();$crud->unset_export();
			$crud->unset_texteditor('cat_desc');	
			
			// log record add
			$crud->field_type('created_by', 'hidden', $this->jCfg['user']['id']);		
			$crud->field_type('created_date', 'hidden', date('Y-m-d H:i:s'));
			$crud->field_type('created_ip', 'hidden', $this->input->ip_address());		
			// log record edit
			$crud->field_type('modified_by', 'hidden', $this->jCfg['user']['id']);		
			$crud->field_type('modified_date', 'hidden', date('Y-m-d H:i:s'));
			$crud->field_type('modified_ip', 'hidden', $this->input->ip_address());
			
			$output = $crud->render();
			$this->_example_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function sub_category()
	{
		try{
			$crud = new grocery_CRUD();
			//$crud->set_theme('datatables');
			$crud->set_table('dam_subcategory');
			$crud->set_relation('sub_cat_id','dam_category','cat_name');
			$crud->set_subject('Sub Category');
			$crud->columns('sub_name','sub_cat_id','sub_icon_id','active','created_date');
			$crud->callback_field('sub_icon_id',array($this,'cb_field_icon'));		
			$crud->callback_column('sub_icon_id',array($this,'_cb_thumb'));
			$crud->callback_column('created_date',array($this,'_cb_date'));
			$crud->callback_column('active',array($this,'_cb_active'));
			$crud->display_as('sub_icon_id','Icon');
			$crud->display_as('sub_cat_id','Category');
			$crud->display_as('sub_name','Name');
			$crud->display_as('sub_desc','Description');
			$crud->required_fields('sub_name','cat_id');
			$crud->unset_add_fields('sub_icon','modified_date','modified_by','modified_ip');			
			$crud->unset_edit_fields('sub_icon','created_date','created_by','created_ip');	
			$crud->unset_read();$crud->unset_export();
			$crud->unset_texteditor('sub_desc');	
			
			// log record add
			$crud->field_type('created_by', 'hidden', $this->jCfg['user']['id']);		
			$crud->field_type('created_date', 'hidden', date('Y-m-d H:i:s'));
			$crud->field_type('created_ip', 'hidden', $this->input->ip_address());		
			// log record edit
			$crud->field_type('modified_by', 'hidden', $this->jCfg['user']['id']);		
			$crud->field_type('modified_date', 'hidden', date('Y-m-d H:i:s'));
			$crud->field_type('modified_ip', 'hidden', $this->input->ip_address());
			
			$output = $crud->render();
			$this->_example_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	public function user()
	{
		try{
			$crud = new grocery_CRUD();
			$crud->set_table('dam_user');
			$crud->set_relation('group_id','dam_group','group_name');
			
			$crud->set_subject('User');
			$crud->columns('user_photo','user_name','user_fullname','group_id','active','created_date');
			$crud->callback_column('created_date',array($this,'_cb_date'));
			$crud->callback_column('active',array($this,'_cb_active'));
			$crud->callback_column('user_photo',array($this,'_cb_photo'));
			$crud->callback_column('user_name', array($this, 'cb_group_access'));
			/*
			add action reset password
			syntax: void add_action( string $label,  string $image_url , string $link_url , string $css_class ,  mixed $url_callback)
			*/
			$crud->add_action('',image_url().'resetpassword16.png','','',array($this,'cb_reset_password'));
			
			$crud->set_rules('group_id','Group','required');
			$crud->set_rules('user_hp', 'Phone',  'regex_match[/^[0-9().-]+$/]');
			$state = $crud->getState();
			$state_info = $crud->getStateInfo();
			if( $state == 'add' || $state== 'insert' || $state == 'insert_validation')
			{
				$crud->set_rules('user_name','Username','required|valid_email|is_unique[dam_user.user_name]|xss_clean');
				$crud->set_rules('user_fullname','Full name','required');
			}
			elseif( $state == 'edit' || $state == 'update' || $state == 'update_validation') 
			{
				$crud->set_rules('user_name','Username','valid_email|xss_clean');
				$crud->set_rules('user_fullname','Full name','required|xss_clean');
				$crud->change_field_type('user_name','readonly');
			}
			
			$crud->set_field_upload('user_photo','files/temp');
			$crud->display_as('user_photo','Photo (4:5)');
			$crud->display_as('user_fullname','Fullname');
			$crud->display_as('user_name','Username');
			$crud->display_as('user_hp','Phone');
			$crud->display_as('group_id','Group');
			$crud->display_as('user_desc','Description');
			$crud->display_as('active','Approve');			
			
			if (get_Initial()->total_user == 1)
			{
				$crud->unset_add();$crud->unset_delete();
				$crud->where('level','administrator') ;
			}
			else if (get_Initial()->total_user > 1)
			{
				$crud->where('level','user');
			}
			
			$crud->unset_add_fields('modified_date','modified_by','modified_ip');			
			$crud->unset_edit_fields('password','created_date','created_by','created_ip');	
			$crud->unset_read(); $crud->unset_export();
			$crud->unset_texteditor('user_desc');
			
			$crud->callback_before_upload(array($this,'_cb_before_upload_photo'));
			$crud->callback_after_upload(array($this,'_cb_after_upload_photo'));
			// log record add
			$crud->field_type('level', 'hidden', 'user');	
			 //set pass using generate token and substring 6 character
			$password = substr(generate_token(32),0,6); //encrypt using sha1
			$crud->callback_after_insert(array($this, '_cb_ai_user')); //after insert send user name & email to email's user 
			//$crud->callback_before_insert(array($this,'_cb_bi_user'));
			$crud->field_type('password', 'hidden',$password);				
			$crud->field_type('created_by', 'hidden', $this->jCfg['user']['id']);		
			$crud->field_type('created_date', 'hidden', date('Y-m-d H:i:s'));
			$crud->field_type('created_ip', 'hidden', $this->input->ip_address());		
			// log record edit
			$crud->field_type('modified_by', 'hidden', $this->jCfg['user']['id']);		
			$crud->field_type('modified_date', 'hidden', date('Y-m-d H:i:s'));
			$crud->field_type('modified_ip', 'hidden', $this->input->ip_address());
			
			$output = $crud->render();
			$this->_example_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}		

	
	function _cb_ai_user($post_array,$primary_key)
	{		
		$email = $post_array['user_name'];
		$pass = $post_array['password'];
		$password = sha1($pass);
		$client_name = get_initial()->client_name; 
		require 'lib/PHPMailer-master/PHPMailerAutoload.php';
		$mail = new PHPMailer();
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = get_smtp()['host'];  					  // Specify main and backup server
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = get_smtp()['username'];                 // SMTP username
		$mail->Password =  get_smtp()['password'];                    // SMTP password
		//$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
		$mail->From = get_smtp()['username'];
		$mail->FromName = 'Admin';
		$mail->addAddress($email);
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = "User Information Magno9 DAMS - $client_name";
		$mail->Body   = "
			Dear User ,<br><br>
				
			username: $email <br>
			password : $pass   <br>			
				
			Best Regards,<br>
			Admin			
		";
		//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		if(!$mail->send()) 
		{
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
			exit;
		}
				
		$dat = array('password'	=> $password);
		$this->db->where('user_name',$email);		
		$up = $this->db->update('dam_user',$dat);
		if ($up)
			{return true;}
		else
			{die('error');}
	}
	
	public function change_password()
	{	
		$this->load->view('admin_menu_v',TRUE);
		$data['content'] = "change_pass_frame";
		$this->load->view('dashboard',$data);
	}
	
	public function cb_reset_password($primary_key , $row)
	{
		$name_url = url_title($row->user_fullname,"-",TRUE);
		$atts = array(
              'width'      => '380',
              'height'     => '210',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '500',
              'screeny'    => '300'
            );		
		//return  anchor_popup("admin/reset_password/$row->user_id/$name_url", '', $atts); 		
		return site_url("admin/reset_password/$row->user_id/$name_url");
	}
	
	public function reset_password()
	{
		$this->load->model('site_mod','S');
		$this->load->model('user_mod','U');
		$data['user_id'] = $user_id = $this->uri->segment(3);
		$user_data = $this->U->get(array('id'=>$user_id))->row();
		$data['user_name'] = $user_data->user_name;
		$data['user_fullname'] = $user_data->user_fullname;
		/*$data['q_group'] = get_group();		
		$data['access'] = $this->S->get_group_access(array('user_id'=>$user_id))->result();
		$this->load->view('group_access_v',$data);*/
	 	//$this->load->view('welcome_message');
		$data['content'] = 'reset_password_v';
		$this->load->view('dashboard',$data);
	}
	
	function reset_password_sendmail()
	{	
		$this->load->model('user_mod','U');
		$user_id = $this->input->get('user_id');
		if($user_id)
		{
			$r = $this->U->get(array('id'=>$user_id))->row();
			$email = $r->user_name;
			$pass = generate_token(6);
			$password = sha1($pass);
						
			$client_name = get_initial()->client_name; 
			// PHP Swift Mailer
			require 'lib/PHPMailer-master/PHPMailerAutoload.php';
			$mail = new PHPMailer();
			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = get_smtp()['host'];  					  // Specify main and backup server
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = get_smtp()['username'];                 // SMTP username
			$mail->Password =  get_smtp()['password'];                    // SMTP password
			//$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
			$mail->From = get_smtp()['username'];
			$mail->FromName = 'Admin';
			$mail->addAddress($email);
			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = "Reset Password Magno9 DAMS - $client_name";
			$mail->Body   = "
			Dear $r->user_fullname,<br>
			<br>	
			Username	 : $email <br>
			New Password : $pass   <br>			
			<br>	
			Best Regards,<br>
			Admin			
			";
			//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			if(!$mail->send()) 
			{
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $mail->ErrorInfo;
				exit;
			}
					
			$dat = array('password'	=> $password);
			$this->db->where('user_name',$email);		
			$up = $this->db->update('dam_user',$dat);
			if ($up)
				{echo "Reset Password Success. Message has been sent. (New Password: $pass)";}
			else
				{die('error');}
		}
	}
	
	

	public function  cb_group_access($value, $row)
	{
		$name_url = url_title($row->user_fullname,"-",TRUE);
		$atts = array(
              'width'      => '380',
              'height'     => '210',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '500',
              'screeny'    => '300'
            );		
		return  anchor_popup("admin/group_access/$row->user_id/$name_url", $value, $atts); 
	}
	
	public function group_access()
	{
		$this->load->model('site_mod','S');
		$this->load->model('user_mod','U');
		$data['user_id'] = $user_id = $this->uri->segment(3);
		$data['user_fullname'] = ucwords(str_replace("-"," ",$this->uri->segment(4)));
		$data['q_group'] = get_group();		
		$data['access'] = $this->S->get_group_access(array('user_id'=>$user_id))->result();
		$this->load->view('group_access_v',$data);
	}
	
	public function group_access_post()
	{		
		//echo "<pre>";print_r($_POST);echo "</pre>";
		$user_id = $this->input->post('user');
		$date = date("Y-m-d H:i:s");
		foreach($_POST['items'] as $row)
		{
			if ($row!='')
			{
				$str = explode("_",$row);
				$qcek_add = $this->S->cek_group_access($user_id,$str[1]);
				if (count($qcek_add)==0)
				{	
					$data_add_ins = array(
						'user_id'	=> $user_id,
						'group_id'	=> $str[1],
						'created_date'	=> $date,
						'created_by'	=> $this->jCfg['user']['id'], 						
						'created_ip'	=> $this->input->ip_address(),
						$str[0]			=> $str[2]							
					);
					$ins = $this->db->insert('dam_group_access',$data_add_ins);
					if (!$ins)	{die('insert db error');}
				}
				else if(count($qcek_add)==1)
				{
					$data_add_upd = array(
						'modified_by'	=> $this->jCfg['user']['id'], 						
						'modified_ip'	=> $this->input->ip_address(),
						$str[0]			=> $str[2]								
					);
					$this->db->where('user_id',$user_id);
					$this->db->where('group_id',$str[1]);
					$upd = $this->db->update('dam_group_access',$data_add_upd);
					if (!$upd){die('update db error');}
				}
			}
		}
		/*
		
		$post_add = $this->input->post('add');
		$date = date("Y-m-d H:i:s");
		if ($post_add)
		{
			foreach($post_add as $add_row)
			{
				$qcek_add = $this->S->cek_group_access($user_id,$add_row);
				if (count($qcek_add)==0)
				{				
					$data_add_ins = array(
						'user_id'	=> $user_id,
						'group_id'	=> $add_row,
						'created_date'	=> $date,
						'created_by'	=> $this->jCfg['user']['id'], 						
						'created_ip'	=> $this->input->ip_address(),
						'add'		=> 1						
					);
					$ins = $this->db->insert('dam_group_access',$data_add_ins);
					if ($ins)
						{echo 'insert db success';}
					else
						{die('insert db error');}
				}
				else
				{
					echo 'udah ada bro';
				}
			}
		}
		//$str = explode("_",$_POST['add_1']); 
		//if ($str[0]
		/*$q = $this->S->cek_group_access($_POST['user_id'],$_POST['group_id']);
		if (count($q)==1)
		{
			echo 'harus diupdate ni..';
		}
		else
		{
			echo 'insert dong..';
			//$this->db->insert('dam_group_access',);
		}*/
		
	}
	
	public function theme()
	{
		$crud = new grocery_CRUD();
			//$crud->set_theme('datatables');
		$crud->set_table('dam_template');
		$crud->set_subject('Theme');			
			$crud->columns('theme_name','theme_preview','active');			
			$crud->callback_column('active',array($this,'_cb_active'));
			
			$crud->required_fields('theme_name','theme_preview','theme_dir');		
			$crud->unset_add_fields('modified_date','modified_by','modified_ip');			
			$crud->unset_edit_fields('created_date','created_by','created_ip');	
			$crud->unset_read();
			$crud->unset_texteditor('theme_desc');
			$crud->display_as('theme_dir','Theme Directory');
			$crud->set_field_upload('theme_preview','files/theme/preview');
			// log record add
			$crud->field_type('created_by', 'hidden', $this->jCfg['user']['id']);		
			$crud->field_type('created_date', 'hidden', date('Y-m-d H:i:s'));
			$crud->field_type('created_ip', 'hidden', $this->input->ip_address());		
			// log record edit
			$crud->field_type('modified_by', 'hidden', $this->jCfg['user']['id']);		
			$crud->field_type('modified_date', 'hidden', date('Y-m-d H:i:s'));
			$crud->field_type('modified_ip', 'hidden', $this->input->ip_address());
			
			$output = $crud->render();
		$this->_example_output($output);
	}
	
	public function file_access()
	{
		$crud = new grocery_CRUD();
			//$crud->set_theme('datatables');
		$crud->set_table('dam_file');
		$crud->set_subject('File access Edit');
		//$crud->set_relation('file_id','dam_file','file_title');
		//$crud->set_relation('user_id','dam_user','user_fullname');
		//$crud->columns('theme_name','theme_preview','active');			
		//$crud->callback_column('active',array($this,'_cb_active'));		
		$crud->set_relation_n_n('users','dam_file_access','dam_user','file_id','user_id','user_fullname');
		//$crud->required_fields('theme_name','theme_preview','theme_dir');		
		$crud->unset_add();$crud->unset_delete();$crud->unset_read();
		$crud->fields('files');						
		// log record edit
		//$crud->field_type('file_id','readonly',1);	
		/*$crud->field_type('modified_by', 'hidden', $this->jCfg['user']['id']);		
		$crud->field_type('modified_date', 'hidden', date('Y-m-d H:i:s'));
		$crud->field_type('modified_ip', 'hidden', $this->input->ip_address());	*/
			
		$output = $crud->render();
		$this->_example_output($output);
	}
	


	
}