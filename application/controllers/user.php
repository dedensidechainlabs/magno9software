<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
    }
	
	public function index()
	{
		$data['title'] = 'Magno9 | Digital Asset Management System';
		$this->load->view('vuserlogin',$data);
	}	
	
	public function login()
	{
		$txtusername =  $this->input->post('txtusername');
		$txtpassword =  $this->input->post('txtpassword');
		$qcekusername = $this->db->query("SELECT * FROM TUSER WHERE USEREMAIL='$txtusername' AND USERPASSWORD='$txtpassword' AND USERLEVEL=2");
		$valcekusername = $qcekusername->num_rows();
		if($valcekusername>0){
			$row = $qcekusername->row();
			$userlevel = $row->USERLEVEL;
			$userid = $row->REFERENCEID;	
			if($userlevel == 2){
				$this->load->library('session');
				$this->session->set_userdata('username', $txtusername);
				$this->session->set_userdata('userlevel', $userlevel);
				$this->session->set_userdata('userid', $userid);
				redirect('user/dashboard','refresh');
			}
		}else{
			//echo "SELECT * FROM TUSER WHERE USEREMAIL='$txtusername' AND USERPASSWORD='$txtpassword'";
			//echo '<script>alert('.$valcekusername.')</script>';
			//echo '<script>alert("Wrong Username or Password! Please Try Again!")</script>';
			//echo "<script>window.history.back();</script>";
			//redirect('user','refresh');
			$data['error'] = "Wrong Username or Password! Please Try Again!";
			$this->load->view("vuserlogin",$data);
		}
		
	}
	
	public function dashboard()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Magno9 | Digital Asset Management System';
			$data['uri1'] = $uri1 = $this->uri->segment(1);
			$data['uri2'] = $uri2 = $this->uri->segment(2);
			$data['uri3'] = $uri3 = $this->uri->segment(3);
			$data['uri4'] = $uri4 = $this->uri->segment(4);
			$userid = $this->session->userdata('userid');
			$this->load->model('muser');
			$data['getuseraffiliatedetail'] = $this->muser->getuseraffiliatedetail($userid);
			$row = $this->muser->getuseraffiliatedetail($userid);
			foreach($row as $row){
				$data['referenceid'] = $row->REFID;
				$data['userid'] = $row->USERID;
				$data['firstname'] = $row->AFFILIATEFIRSTNAME;
				$data['lastname'] = $row->AFFILIATELASTNAME;
				$data['email'] = $row->AFFILIATEEMAIL;
				$data['address'] = $row->AFFILIATEADDRESS;
				$data['state'] = $row->AFFILIATESTATE;
				$data['city'] = $row->AFFILIATECITY;
				$data['postalcode'] = $row->AFFILIATEPOSTALCODE;
				$data['phone'] = $row->AFFILIATEPHONE;
				$data['submitdate'] = date("d M Y",strtotime($row->SUBMITDATE));
				$data['username'] = $row->USEREMAIL;
				$data['password'] = $row->USERPASSWORD;
			}
			$this->load->view('vuserdashboard',$data);
		}else{
			redirect('user','refresh');
		}
	}
	
	public function affiliatedetail()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Magno9 | Digital Asset Management System';
			$data['uri1'] = $uri1 = $this->uri->segment(1);
			$data['uri2'] = $uri2 = $this->uri->segment(2);
			$data['uri3'] = $uri3 = $this->uri->segment(3);
			$data['uri4'] = $uri4 = $this->uri->segment(4);
			$userid = $this->session->userdata('userid');
			$data['userid'] = $userid;
			$this->load->model('muser');
			$data['getuseraffiliatedetail'] = $this->muser->getuseraffiliatedetail($userid);
			$this->load->view('vuseraffiliatedetail',$data);
		}else{
			redirect('user','refresh');
		}
	}
	
	function profile()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Magno9 | Digital Asset Management System';
			$data['uri1'] = $uri1 = $this->uri->segment(1);
			$data['uri2'] = $uri2 = $this->uri->segment(2);
			$data['uri3'] = $uri3 = $this->uri->segment(3);
			$data['uri4'] = $uri4 = $this->uri->segment(4);
			$userid = $this->session->userdata('userid');
			$this->load->model('muser');
			$data['getuseraffiliatedetail'] = $this->muser->getuseraffiliatedetail($userid);
			$row = $this->muser->getuseraffiliatedetail($userid);
			foreach($row as $row){
				$data['referenceid'] = $row->REFID;
				$data['userid'] = $row->USERID;
				$data['firstname'] = $row->AFFILIATEFIRSTNAME;
				$data['lastname'] = $row->AFFILIATELASTNAME;
				$data['email'] = $row->AFFILIATEEMAIL;
				$data['address'] = $row->AFFILIATEADDRESS;
				$data['state'] = $row->AFFILIATESTATE;
				$data['city'] = $row->AFFILIATECITY;
				$data['postalcode'] = $row->AFFILIATEPOSTALCODE;
				$data['phone'] = $row->AFFILIATEPHONE;
				$data['submitdate'] = date("d M Y",strtotime($row->SUBMITDATE));
				$data['username'] = $row->USEREMAIL;
				$data['password'] = $row->USERPASSWORD;
			}
			$this->load->view('vuserprofile',$data);
		}else{
			redirect('user','refresh');
		}
	}
	function logout()
	{
		$this->session->sess_destroy();
		redirect('user');
	}
}