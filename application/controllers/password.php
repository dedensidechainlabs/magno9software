<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Password extends MD_Controller { //Don't change this line
	 
	public function __construct()
	{
		parent::__construct();			
		$this->load->model('user_mod','U');			
	}
	
	function index()
	{	
		$this->change();
	}
	
	public function change()
	{
		$this->load->view('change_password_v');
	}
	
	public function post()
	{
		$this->load->model('user_mod','U');
		if ($this->input->post('submit'))
		{
			$user_id = $this->jCfg['user']['id'];
			$user_name = $this->input->post('user_name');
			$old_password = $this->input->post('old_password');		
			$new_password = $this->input->post('new_password');					
			$new_password_conf = $this->input->post('new_password_conf');			
			$p = array(
				'user_name'		=>	$user_name, 
				'password'		=>	$old_password
			);
			$q = $this->U->cek($p)->row();			
			if (count($q)< 1)
			{
				$data['error'] = 'old password wrong';				
			}
			else
			{
				if ($new_password =='' AND $new_password_conf=='')
				{
					$data['error'] = 'New password & confirmation required';					
				}				
				else
				{
					$dat = array(
						'password'	=>  sha1($new_password)
					);
					$this->db->where('user_id',$user_id);
					$q_up = $this->db->update('dam_user',$dat);
					if ($q_up)
					{			
						$data['success'] = 'Change Password Success';							
					}
					else
					{
						$data['error'] = 'Change password failed, Please Try again';	
					}
				}
			}
		}	
		$this->load->view('change_password_v',$data);
	}
}