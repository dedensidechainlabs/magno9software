<script src="<?php echo asset_url()?>jquery-validation-1.11.1/jquery.validate.js" type="text/javascript"></script>

<div class="add-edit-cont">
	
	<form id="f_addfile" action="<?php echo base_url()?>file/add_edit_post/edit" method="post">
	<input type="hidden"  name="file_id" id="file_id" value="<?php echo $file_id ?>">
	<table>
		<colgroup>
			<col style="width:25%">
			<col style="width:75%">
		</colgroup>
		<tr>
			<td><label for="file_title" >Title </label></td>
			<td><input type="text" name="file_title" id="file_title" value="<?php echo (isset($file_title)?$file_title:"")?>" required></td>
		</tr>
		<tr>
			<td><label for="asset_code" >Asset Code</label></td>
			<td><input style="color:#8d8d8d;background-color:#dddddd;" type="text" name="file_code" value="<?php echo $file_code;?>" readonly></td>
		</tr>
		<tr>
			<td><label for="file_desc" >Description</label></td>
			<td><textarea name="file_desc" required><?php echo (isset($file_desc)?$file_desc:"")?></textarea></td>
		</tr>
		<tr>
			<td><label for="file_keyword" >Keyword</label></td>
			<td><input maxlength="140" type="text" name="file_keyword" value="<?php echo (isset($file_keyword)?$file_keyword:"")?>" required></td>
		</tr>
		<tr>
			<td><label for="file_created_date" >Created Date</label></td>
			<td><input maxlength="140" type="text" name="file_created_date" id="datepick" value="<?php echo (isset($file_created_date)?$file_created_date:"")?>"></td>
		</tr>
		<tr>
			<td><label for="file_creator" >Creator</label></td>
			<td><input maxlength="140" type="text" name="file_creator" value="<?php echo (isset($file_creator)?$file_creator:"")?> "></td>
		</tr>
		<tr height="100">
			<td><label for="file_thumbnail" >Thumbnail</label></td>
			<td><?php echo $file_thumbnail_cont?></td>
		</tr>
		<tr height="100">
			<td><label for="file_prev" >Preview</label></td>
			<td><?php echo $file_prev_cont?></td>
		</tr>
		<tr>
			<td><label for="file_ori" >Original</label></td>
			<td>
				<?php echo $file_ori?>
				<input type="hidden" name="file_ori" id="name-ori" value="<?php echo $file_ori; ?>"  />				
				<input type="hidden" value="<?php echo $file_extension; ?>" name="file_extension" id="ext-ori" />
			</td>
		</tr>
		<tr>
			<td><label for="is_private" >Private</label></td>
			<td>
				<input  type="checkbox" id="private_is" name="private_is" value="" <?php echo $check; ?> >
				<div id="private"  <?php if($check==''){echo "style='display:none'";}?>>
					<input type="text" id="share_user" name="blah2" value=""/>
					<!--<input type="button" value="Submit" />-->
					
				</div>
				<input type="hidden" id="is_private" name="is_private" value="">
				<input type="hidden" id="user_private" name="user_private" value="<?php echo (isset($user_private)?$user_private:"") ?>">
			</td>
		</tr>
		<tr>
			<td style="border:1 px solid #cbcbcb;" colspan="2">
			<input name="submit" id="save" type="submit" value="Submit" class="btn btn-primary"></td>
		</tr>
	</table>	
	
</form>
</div>
<?php $this->load->view('group_cat_sub_seljs'); ?>
<script>
	category();
	
	$(function() {
		$( "#datepick" ).datepicker(
			{ dateFormat: "yy-mm-dd" }
		);
	});
	//is_cek();
		$("#f_addfile").validate();		
		
		$("#save").click(function(){
			var thumbnail = $("#frame-thumbnail").contents().find("#thumbnail").html();								
			if (thumbnail)
			{
				$("#name-thumbnail-asli").val(thumbnail);	
				$("#name-thumbnail").val(thumbnail);	
			}
			else
			{	
				$("#name-thumbnail").val('<?php echo $file_thumbnail; ?>');
			}
			
			var pre = $("#frame-prev").contents().find("#prev").html();								
			if (pre)
			{
				$("#name-prev-asli").val(pre);	
				$("#name-prev").val(pre);	
			}
			else
			{	
				$("#name-prev").val('<?php echo $file_prev; ?>');
			}
			
			
			var ori = $("#frame-ori").contents().find("#ori").html();
		
			if (ori)
			{
				$("#name-ori").val(ori);
			}
			else
			{	
				$("#name-ori").val('<?php echo $file_ori; ?>');
			}
			
			var n=ori.split(".");
			var arr_length = n.length;
			$("#ext-ori").val(n[arr_length-1]);
		});
		
		$("#thumbnail_klik").click(function(){
			alert("bla");
			$("#thumbnail_cont").fadeOut(10,function(){
				$("#frame-thumbnail").fadeIn();
			});
		});	
		
		$("#prev_klik").click(function(){
			alert("bla");
			$("#prev_cont").fadeOut(10,function(){
				$("#frame-prev").fadeIn();
			});
		});	
		
		$("#ori_klik").on("click",function(){
			$("#ori_cont").fadeOut(10,function(){
				$("#frame-ori").fadeIn();
			});
		});
		

	
	$("#private_is").click(function(){
		if ($(this).is(":checked")){
			$("#is_private").val("1");
			$("#private").show();
		}
		else{
			$("#is_private").val("0");
			$("#private").hide();
		}	
	});
	
	$("#share_user").tokenInput("<?php echo base_url()?>user/get",{
		theme: "facebook",
		preventDuplicates: true,
		//prePopulate:  $("#blason").html()
		prePopulate: <?php echo $pre_populate; ?>
	});
	
	
	$("#share_user").on("change",function(){
		var su = $(this).val();
		$("#user_private").val(su);
	});
		
</script>