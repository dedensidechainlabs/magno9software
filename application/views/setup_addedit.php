<!--
Package Client Add view
Service Add view
-->

<div class="h2-rale"> Client Registration </div>
	<form id="setup_form"  action="<?php echo base_url()."site/client_registration_post"?>" method="post">
		<table>
			<colgroup>
				<col style="width:20%;"></col>
				<col style="width:80%;"></col>
			</colgroup>
			<input type="hidden" name="master_key" value="<?php echo $master_key ?>">
			<tr>
				<td>Client </td>
				<td>
					<select name="client_id" required>
						<option value="">- Select -</option>
					<?php foreach ($q_client as $row)
					{	
						$qcek = $this->S->get_package_client(array('pack_client_id'=>$row->client_id))->row();	
						if(count($qcek)==0)
						{
							?>
							<option value="<?php echo $row->client_id ?>"><?php echo $row->client_name; ?></option>															
							<?php
						}
					}	?>
					</select>	
					<a href="<?php echo site_url("client_master/add")?>" class="btn" >New</a> 				
				</td>
			</tr>
			<tr>
				<td>Domain</td><td><input type="text" name="domain" value="<?php echo isset($domain_name)?$domain_name:""?>" required></td>
			</tr>
			<tr>
				<td>Hosting </td><td><input type="text" name="hosting"  value="<?php echo isset($hosting)?$hosting:""?>" required></td>
			</tr>
			<tr>
				<td>Start Contract</td><td><input type="text" id="datepicker1" name="start_contract" value="<?php echo isset($start_contract)?$start_contract:""?>" required></td>
			</tr>
			<tr>
				<td>End Contract</td><td><input type="text" id="datepicker2" name="end_contract" value="<?php echo isset($end_contract)?$end_contract:""?>" required></td>
			</tr>
			<tr>
				<td colspan="2"> <b>Package</b></td>
			</tr>
			<tr>
				<td colspan="2">
					<table width="300">
						<?php 
						foreach ($q_package as $row)
						{	?>
							<tr>							
								<td><input type="radio" name="pack_id" value="<?php echo $row->pack_id ?>" ></td>
								<td><?php echo $row->pack_name;?> </td>
								<td><?php echo $row->pack_max_user; ?> user  </td>
								<td><?php echo $row->pack_max_storage;?> GB</td>
								<td>Rp. <?php echo number_format($row->pack_monthly_fee,0,',','.');?></td>									
							</tr>								
							<?php
						}	?>							
					</table>					
				</td>
			</tr>
			<tr height="30" ></tr>
			<!-- Service -->
			<tr>
				<td colspan="2"> <b>Service</b></td>
			</tr>
			<tr>
				<td colspan="2">
					<table border="1" width="200">
					<tr>
						<td><input type="checkbox" class="cek-all" ></td>						
						<th>Desc</th>
						<th>Price (bulan)</th>
						<th>Quantity</th>
						<th>Total</th>
					</tr>
					<?php foreach ($q_service as $row)
					{
						if($row->service_max_storage=='')
						{
							$qt = "type='hidden' id='qty_$row->service_id' value='0'";
						}
						else
						{
							$qt = "type='number' max='99' maxlength='2' size='2' min='1' id='qty_$row->service_id'  value='1'";
						}
						?>
						<tr>
							<td>														
								<!--<input type="hidden" name="service[]"  >-->
								<input type="checkbox" class="serv_id" id="ser_<?php echo $row->service_id ?>" value="<?php echo $row->service_id ?>"  >
								<input type="hidden" class="ceks" value="0"  id="ceks_<?php echo $row->service_id ?>" >
							</td>
							<td>
								<?php echo $row->service_name;?> 			
								<?php 
								if ($row->service_max_storage==0)
									{$serv_ms = '';}
								else
									{$serv_ms = $row->service_max_storage." GB";}	
								echo $serv_ms;
								?> 
								<?php echo $row->service_desc;?>
								<textarea style="display:none" id="serv_desc_<?php echo $row->service_id ?>" ><?php echo "$row->service_name $serv_ms $row->service_desc";?></textarea>								
							</td>
							<td>
								<input type="hidden" id="service_fee_<?php echo $row->service_id;?>" value="<?php echo $row->service_monthly_fee;?>">
								<?php echo number_format($row->service_monthly_fee,0,',','.');  ?>
							</td>						
							<td><input style='width:40px;margin:0;padding:0;' class='serv-qty' <?php echo $qt; ?> ></td>	
							<td>
								<input style='width:100px;margin:0;padding:0;' type='text' class='tprice' id='total_price_<?php echo $row->service_id;?>' value='<?php echo $row->service_monthly_fee; ?>' readonly>	
								<textarea style="display:none" class="service" name="service[]" id="service_<?php echo $row->service_id ?>"  ></textarea>
							</td>
						</tr>
						<?php
					}	?>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2"> 
					<input type="submit" class="btn btn-primary" id="sub" name="submit" value="Submit">
				</td>
			</tr>
		</table>
	</form>
	
	<script>
		tPrice();	
		
		function tPrice() {
			$(".serv_id").each(function(){
				var Id = $(this).val();					
				cekId(Id);
				total(Id);
			});			
		}
		
		function total(parId)
		{
			var desc = $("#serv_desc_"+parId).val();
			var ceks = $("#ceks_"+parId).val();
			var qty = $("#qty_"+parId).val();		
			var ser = $("#service_fee_"+parId).val();		
			if(qty==0)
			{
				var serqty = ser;	
			}
			else
			{
				var serqty = ser * qty;	
			}
			$("#total_price_"+parId).val(serqty);
			$("#service_"+parId).val(ceks+"_"+parId+"_"+qty+"_"+serqty+"_"+desc);	
		}
		
		function cekId(par)
		{
			$("#ser_"+par).on("click",function(){
				if ($(this).prop('checked')==true)
				{				
					$("#ceks_"+par).val(1);
				}
				else
				{
					$("#ceks_"+par).val(0);
				}
				total(par);
			});
		}
		
		function cekAlltotal()
		{
			$(".ceks").each(function(){			
				var Id = $(this).attr("id");
				var spId = Id.split("_");
				total(spId[1]);
			});
		}
		
		$(".serv-qty").on("change",function(){		
			tPrice();
		});
		
		// cek all service
		$(".cek-all").click(function(){
			if ($(this).prop('checked')==true)
			{
				$(".serv_id").prop('checked',true);
				$(".ceks").val(1);
			}
			else
			{
				$(".serv_id").prop('checked', false);
				$(".ceks").val(0);	
			}
			cekAlltotal();
		});
		
		$(function() {
			$( "#datepicker1" ).datepicker({dateFormat: "yy-mm-dd"});
			$( "#datepicker2" ).datepicker({dateFormat: "yy-mm-dd"});	
			$("#setup_form").validate();	
		});
		
		$("#pack_id_master").click(function(){	
			var idm = $(this).val();
			alert(idm);
			/*if ($(this).prop('checked')==true)
			{
				
				$.get("<?php echo base_url;?>site/get_pack/?id="+id,function(data,status){
					alert(status);
					//$("#pack_description").text(data);
				});				
			}*/
		});
	</script>