<form id="fsearch" action="<?php echo site_url("report/excel");?>" method="get">
	<table>	
		<tr>
			<td><label for="group_id" >Group</label></td>
			<td>
				<select name="group_id" id="group_id">
					<option value="">-- All Group --</option>
					<?php 
					$par = array('view'=>1);
					$qbla = get_group($par); 					
					?>
					<?php 
					foreach ($qbla as $row)
					{
						if ($this->input->get('group_id') == $row->group_id)
							{$sel = "selected";}
						else
							{$sel = "";}
						echo "<option value='$row->group_id' $sel>$row->group_name</option>";
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td><label for="cat_id" >Category</label></td>
			<td><select name="cat_id" id="cat_id">
					<!--<option value="">-- All Category --</option>-->
					<?php 
					/*$qcat = get_category(); 					
					?>
					<?php 
					foreach ($qcat as $row)
					{		
						if ($this->input->get('cat_id') == $row->cat_id)
							{$sel = "selected";}
						else
							{$sel = "";}
						echo "<option value='$row->cat_id' $sel >$row->cat_name</option>";
					}*/
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td><label for="sub_id" >Sub Category</label></td>
			<td>
				<select name="sub_id" id="sub_id">
					<!--<option value="">-- All Sub Category --</option>-->
					<?php 
					/*$qsub = get_subcategory(); 		
					foreach ($qsub as $row)
					{		
						if ($this->input->get('sub_id') == $row->sub_id)
							{$sel = "selected";}
						else
							{$sel = "";} 
						echo "<option value='$row->sub_id' $sel>$row->sub_name</option>";
					}*/
					?>
				</select>
			</td>
		</tr>
	</table>	
	<span style="float: left;width: 90px;margin-left: 5px;padding-top: 5px;">Year</span>
	<select name="year" id="year">
		<option value="">-- All Year --</option><?php
					$qyear_sel = get_file_yearlist();				
					foreach ($qyear_sel as $rw)
					{
						if ($rw->thn == $this->input->get('year'))
						{
							echo "<option value='$rw->thn' selected>$rw->thn</option>";				
						}
						else
						{
							echo "<option value='$rw->thn'>$rw->thn</option>";
						}
					}
					?>
	</select>
	<br/><br/>
	<input class="btn" type="submit" name="submit" value="Export to Excel">
</form>
<br/><br/>
<div class="error"><?php echo (isset($empty)?$empty:"") ?></div>
<?php $this->load->view('group_cat_sub_seljs'); ?>
<script> 
	category();
	subCategory();
	/*var gr = $("#group_id").val();
	var cat = $("#cat_id:selected").val();
	$.get("<?php echo base_url()?>site/get_cat?gr="+gr+"&id="+cat+"vw=all",function(data){
		$("#cat_id").html(data);
	});*/
</script>