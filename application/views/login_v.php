<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MAGNO9 - Digital Asset Management System </title> 
	<meta name="description" content="DAMS">
	<meta name="keywords" content="DAMS">

	<!--<link href="<?php echo image_url()?>dams_icon.ico" rel="icon" type="image/x-icon" />-->
	<link href="<?php echo asset_url()?>css/template1-login.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo asset_url()?>js/jquery-2.0.2.js" type="text/javascript"></script>
</head>
<body style="background: url(images/bg.png) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">



<?php // echo "<pre>";print_r($this->session->userdata);echo "</pre>"; ?>

<div class="container">
	<div class="text">
		<img src="<?php echo base_url();?>images/magnodams_logo.png" width="300px" style="margin-top:-50px;"><br>
		<h1 style="color:#5c5c5c; font-size: 18px;">Digital Asset Management System<br>Simplify, Organize and Secure your Data Asset Management in a few step</h1>
	</div>
	<div class="step"></div>
	<div class="login">
		
		<form id="flogin" action="<?php echo site_url("login".$url) ?>" method="post">
			<br/><br/><br/><br/>
			<input type="text" name="user_name" placeholder="User Name" value="<?php echo (isset($user_name)?$user_name:"")?>"  ><br>
			<input type="password" name="password"  value="" placeholder="Password"   >
			
			<button  type="submit" style="display:none" id="btn-login" >Login</button>
			<div style="clear:both;"></div>
			<div class="error_text"><?php echo (isset($error)?$error:"")?></div>	
		</form>
		<div class="text-login">Login</div>
	</div>
	<div class="company">
		<div class="name" style="color: #333333"><?php echo get_Initial()->client_name;?></div>
	</div>
</div>
<div class="clr"></div>
<footer>
	<div class="cont">
		<div class="l"  style="color: #333333">Copyrights <?php echo date("Y")?>. All Rights Reserved PT. Mitraguna Adikriya </div>
		<div class="r" ><a href="#"  style="color: #333333">Terms & Usage </a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#"  style="color: #333333">Privacy Policy </a></div>
		<div class="clr"></div>
	</div>
</footer>
</body>

</html>
<script>
	$(".text-login").click(function(){
		$('form').submit();
	});
	
	$(document).keypress(function(e) {
		if(e.which == 13) {
			$('form').submit();
		}
	});
</script>