<script>
	
	function group()
	{
		var gr = $("#group_id:selected").val();
		var add = $("#add").text();
		var view = $("#view").text();
		$.get("<?php echo base_url()?>site/get_group?group_id="+gr+"&add="+add+"&view="+view,function(data){
			$("#group_id").html(data);
		});
	}	
	
	function category()
	{
		var gr = $("#group_id").val();
		var cat = $("#cat_id:selected").val();
		$.get("<?php echo base_url()?>site/get_cat?gr="+gr+"&id="+cat,function(data){
			$("#cat_id").html(data);
		});	
	}
	
	function subCategory()
	{
		var gr = $("#group_id").val();		
		var cat = $("#cat_id:selected").val();
		//var sub = $("#sub_id:selected").val();
		$.get("<?php echo base_url()?>site/get_sub?cat="+cat+"&gr="+gr,function(data){
			$("#sub_id").html(data);
		});	
	}
		
	$("#group_id").on("change", function(){
		var gr = $(this).val();
		$.get("<?php echo base_url(); ?>site/get_cat?gr="+gr, function(data,status){
			$("#cat_id").html(data);
		});			
		$.get("<?php echo base_url(); ?>site/set_code?group_id="+gr, function(data,status){
			$("#file_code").val(data);
		});			
		return false;
	});
		
		$("#cat_id").on("change", function(){
			var cat = $(this).val();
			$.get("<?php echo base_url(); ?>site/get_sub?cat="+cat, function(data,status){
				$("#sub_id").html(data);
			});
			return false;
		});	
</script>