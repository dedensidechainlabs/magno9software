<style>
table {
  border-collapse: collapse;
}
td,th{
	padding:5px;
}
.head{
	background-color:#D2D3D5;
}
* {
	font-family: Arial;
	font-size:14px;
}
</style>

<script src="<?php echo asset_url();?>js/jquery-2.0.2.js" type="text/javascript"></script>
<table border="1">
<tr class="head">
	<td>Access</td>
	<td>Add</td>
	<td>Edit</td>
	<td>Delete</td>
	<td>Download</td>
	<td>View</td>
</tr>
<?php foreach($q_group as $v):?>
<tr>
	<td><input style="display:none" value="<?php echo $v->group_id?>" id="id<?php echo $v->group_id?>"> <?php echo $v->group_name;?></td>
	<td><input class="checkall" id="add_<?php echo $v->group_id;?>" type="checkbox"></td>
	<td><input class="checkall" id="edit_<?php echo $v->group_id;?>" type="checkbox"></td>
	<td><input class="checkall" id="delete_<?php echo $v->group_id;?>" type="checkbox"></td>
	<td><input class="checkall" id="download_<?php echo $v->group_id;?>" type="checkbox"></td>
	<td><input class="checkall" id="view_<?php echo $v->group_id;?>" type="checkbox"></td>
</tr>
<?php endforeach;?>
</table>
<div   style="display:none" id="result"></div>
<br>
<input type="submit" value="Update" id="save">
<script>
	$("#save").click(function(){
		//alert($(this).text());
		var items = [];
		$(".checkall").each(function(n){
			if ($(this).is(":checked")){
				var bli = 1;
			}
			else {
				var bli = 0
			}
			items[n] = $(this).attr("id")+"_"+bli;			
		});
		
		$.post("<?php echo base_url();?>admin/group_access_post",{items: items,user:<?php echo $user_id ?>},function(data){
			$("#result").html(data);
			alert("update success");
		});
	});
	<?php 
	foreach($access as $v)
	{	?>
		$("#add_<?php echo $v->group_id;?>").prop('checked',<?php echo $v->add;?>);
		$("#edit_<?php echo $v->group_id;?>").prop('checked',<?php echo $v->edit;?>);	
		$("#delete_<?php echo $v->group_id;?>").prop('checked',<?php echo $v->delete;?>);	
		$("#download_<?php echo $v->group_id;?>").prop('checked', <?php echo $v->download;?>);
		$("#view_<?php echo $v->group_id;?>").prop('checked',<?php echo $v->view;?>);
		<?php
	}	?>
</script>
