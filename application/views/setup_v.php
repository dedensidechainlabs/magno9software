<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MAGNO9 - Digital Asset Management System </title> 
	<meta name="description" content="DAMS">
	<meta name="keywords" content="DAMS">

	<!--<link href="<?php echo image_url()?>dams_icon.ico" rel="icon" type="image/x-icon" />-->
	<link href="<?php echo asset_url()?>css/template1-setup.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo asset_url()?>js/jquery-2.0.2.js" type="text/javascript"></script>
	<script src="<?php echo asset_url()?>jquery-validation-1.11.1/jquery.validate.js" type="text/javascript"></script>
</head>
<body>

<div class="logo"></div>
<header></header> 
<?php // echo "<pre>";print_r($this->session->userdata);echo "</pre>"; ?>

<div class="container">
	
	<div class="login">
		
		<form id="flogin" action="<?php echo base_url()?>auth/setup_post" method="post">
			<br/><br/><br/>
			<input type="text" maxlength="6" name="master_key" placeholder="Master Key" value="<?php echo (isset($master_key)?$master_key:"")?>"  required>
			<input type="text" maxlength="30" name="user_name" placeholder="Admin Username" value="<?php echo (isset($user_name)?$user_name:"")?>"  required>
			<input type="text" maxlength="30" name="user_fullname" placeholder="Admin Fullname" value="<?php echo (isset($user_fullname)?$user_fullname:"")?>"  required>
			<input type="password" maxlength="15" id="password" name="password"  placeholder="Password"   required>
			<input type="password" maxlength="15"  name="password_conf"  placeholder="Confirm Password"   required>
			<input type="text" name="client_name" placeholder="Company Name" value="<?php echo (isset($client_name)?$client_name:"")?>"  required>
			<br/>
			<input  class="btn" type="submit" name="submit" value="Setup"  >
			<br/>
			<!--<div class="error_text"><?php echo (isset($error)?$error:"")?></div>-->	
		</form>
	</div>
</div>
<div class="clr"></div>
<footer>
	<div class="cont">
		<div class="l">Copyrights <?php echo date("Y")?>. All Rights Reserved PT. Mitraguna Adikriya </div>
		<div class="r"><a href="#">Terms & Usage </a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#">Privacy Policy </a></div>
		<div class="clr"></div>
	</div>
</footer>
</body>

</html>
<script>
	$(".text-login").click(function(){
		$('form').submit();
	});
	
	$(document).keypress(function(e) {
		if(e.which == 13) {
			$('form').submit();
		}
	});
	
	
	/*$("#flogin").validate({
		rules: {			
			password: {required: true, minlength: 4},
			password_conf: {
				equalTo: "#password"
			}
		}
	});*/
	
	$(document).keypress(function(e) {
		if(e.which == 13) {
			$('form').submit();
		}
	});
</script>