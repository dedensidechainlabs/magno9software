<?php 
if (isset($query) && is_array($query)==TRUE)
{	?>
	Sort By<br/>
	<select id="sort">			
		<option value="date" <?php if($this->input->get('order')=='date'){echo "selected";}?> >Created Date</option>
		<option value="name" <?php if($this->input->get('order')=='name'){echo "selected";}?>>Name</option>
	</select>

	<br/>
	<div class="pagination"><?php echo $links ?></div>
	<?php
	foreach($query as $row)
	{		
		$url_title = url_title($row->file_title,"-",TRUE);		
		$cat_id = $this->S->get_subcategory(array('id',$row->sub_id))->row()->cat_id;		
		$group_id_uri  = $this->S->get_category_id($cat_id)->row()->group_id;
		//echo "<pre>";print_r($group_id_uri);echo "</pre>";
		$url_complete = $group_id_uri.'_'.$cat_id.'_'.$row->sub_id.'_'.$row->file_id;
		?>
		<div class="list-thumb" id="<?php echo $row->file_id;?>">
			<a href="<?php echo site_url("file/detail/$url_complete/$url_title")?>">	 
				<div class="nail">
				<?php
				if ($row->file_prev=='' OR file_exists("files/preview/thumbnail/$row->file_prev")==0)	
				{		
					$qex_type = $this->S->get_extension(array('ext'=>$row->file_extension))->row()->file_type; 										
					$qtype_img = $this->S->get_file_type(array('type'=>$qex_type))->row()->filetype_img;?>
					<img class="gbr" src="<?php echo image_url()."$qtype_img"?>">
					<?php
				}
				else
				{	?>
					<img class="gbr" src="<?php echo file_url().'preview/thumbnail/'.$row->file_prev?>">
					<?php
				}
				?>
				</div>
				<div class="title"><?php echo $row->file_title ?></div>
			</a>
			<div  style="width:150px;margin:auto;margin-top:-100px;position:absolute;display:none" class="crud" id="crud_<?php echo $row->file_id;?>">
				<a class="edit" href="<?php echo site_url("file/edit/$url_complete/$url_title")?>"> 
					<img width="24" height="24" src="<?php echo image_url();?>edit.png" />
				</a>
				<a class="delete" id="<?php echo $row->file_id ?>"   href="#<?php echo $row->file_id ?>#file_id#dam_file">
					<img src="<?php echo image_url();?>close.png" />
				</a>					
			</div>							
		</div>	
		<?php
	}
}
else
{
	echo "<h1>$error</h1>";
}
?>

<?php $this->load->view('file_search_list_js');?>



