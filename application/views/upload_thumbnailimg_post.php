<link href="<?php echo base_url()?>static/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js" type="text/javascript"></script>

<!---->
<div  style="display:none" id="thumbnail" ><?php echo $file_name?></div>
<?php 
$ext=preg_replace("/.*\.([^.]+)$/","\\1", $file_name);
if($ext=='jpg' OR $ext=='png'){
?>
<a class="btn" href="<?php echo base_url()?>site/upload_thumbnail/?url=<?php echo $file_name?>">
	<img width="70" height="70" src='<?php echo file_url()."thumbnail/thumbnail/$file_name"?>'>	
</a>
<?php }else if($ext=='mp3'){ ?>

	<audio controls>
		<source src="<?php echo base_url()?>files/thumbnail/temp/<?php echo $file_name?>" type="audio/mpeg">
	</audio>
	<a class="btn" href="<?php echo base_url()?>site/upload_thumbnail/?url=<?php echo $file_name?>">Delete</a>
<?php }else if($ext=='mp4'){ ?>

	<video width="240" height="auto" controls>
		<source src="<?php echo base_url()?>files/thumbnail/temp/<?php echo $file_name?>" type="video/mp4">
	</video>
	<a class="btn" href="<?php echo base_url()?>site/upload_thumbnail/?url=<?php echo $file_name?>">Delete</a>
</a>
<?php } ?>