<script src="<?php echo asset_url()?>jquery-validation-1.11.1/jquery.validate.js" type="text/javascript"></script>
<div style="display:none" id="add">1</div>
<div class="add-edit-cont">
	<?php $this->load->view('upload_v');?><br/>
	<form id="f_addfile" action="<?php echo base_url()?>file/add_edit_post/add" method="post">	
	<table>
		<colgroup>
			<col style="width:25%">
			<col style="width:75%">
		</colgroup>
		<tr>
			<td colspan="3">
				<?php $this->load->view('group_cat_sub_sel'); ?>
			</td>								
		</tr>
		<tr>
			<td><label for="file_title" >Title </label></td>
			<td><input type="text" maxlength="20" name="file_title" id="file_title" value="<?php echo (isset($file_title)?$file_title:"")?>" required></td>
		</tr>
		<!--<tr>
			<td><label for="asset_code" >Asset Code</label></td>
			<td><input style="color:#8d8d8d;background-color:#dddddd;" type="text" id="file_code" name="file_code" value="<?php //echo (isset($file_code)?$file_code:"")?>" readonly></td>
		</tr>-->
		<tr>
			<td><label for="file_desc" >Description</label></td>
			<td><textarea name="file_desc" required><?php echo (isset($file_desc)?$file_desc:"")?></textarea></td>
		</tr>
		<tr>
			<td><label for="file_keyword" >Keyword</label></td>
			<td><input maxlength="140" type="text" name="file_keyword" value="<?php echo (isset($file_keyword)?$file_keyword:"")?>" required></td>
		</tr>
		<tr>
			<td><label for="file_created_date" >Created Date</label></td>
			<td><input maxlength="140" type="text" name="file_created_date" id="datepick" value="<?php echo date("Y-m-d");?>" required></td>
		</tr>
		<tr>
			<td><label for="file_creator" >Creator</label></td>
			<td><input maxlength="140" type="text" name="file_creator" value="<?php echo $this->jCfg['user']['fullname']; ?> "></td>
		</tr>
		<tr height="100">
			<td><label for="file_thumbnail" >Thumbnail</label></td>
			<td>
				<iframe id="frame-thumbnail" style='height:90px;' src="<?php echo base_url()?>site/upload_thumbnail" ></iframe>
				<input type="hidden"  name="file_thumbnail" id="name-thumbnail" />
			</td>
		</tr>
		<tr height="100">
			<td><label for="file_prev" >Preview</label></td>
			<td>
				<iframe id="frame-prev" style='height:90px;' src="<?php echo base_url()?>site/upload_prev" ></iframe>
				<input type="hidden"  name="file_prev" id="name-prev" />
			</td>
		</tr>
		<tr height="80">
			<td><label for="file_ori" >Original</label></td>
			<td>
				<iframe id="frame-ori" style="height:70px;" src="<?php echo base_url()?>site/upload_ori" ></iframe>
				<input type="hidden" placeholder="Original File Name" name="file_ori" id="name-ori" value="" readonly required/>
				<input type="hidden" value="" name="file_extension" id="ext-ori" />
			</td>
		</tr>
		<tr>
			<td><label for="is_private" >Private</label></td>
			<td>
				<input type="checkbox" id="private_is" name="private_is" value="">
				<!--<input type="radio" id="is_private" name="is_private" value="0" checked>N-->	
				<div id="private" style="display:none;">
					<input type="text" id="share_user" name="blah2" value=""/>
					<!--<input type="button" value="Submit" />-->
					
				</div>
				<input type="hidden" id="is_private" name="is_private" value="">
				<input type="hidden" id="user_private" name="user_private" value="">
			</td>
		</tr>
		<tr>
			<td style="border:1 px solid #cbcbcb;" colspan="2">
			<input name="submit" id="save" type="submit" value="Submit" class="btn btn-primary"></td>
		</tr>
	</table>	
	
</form>
</div>

<script>	
	group();
	// subCategory();
	$(function() {
		$( "#datepick" ).datepicker({ 
			dateFormat: "yy-mm-dd", 
			maxDate: 0 
		});
	});	
	
	$("#f_addfile").validate({
		ignore: "",
		rules: {
			file_ori: {
				required:true
			}
		}
	});
	
	
	
	$("#save").click(function(){
		var pre = $("#frame-thumbnail").contents().find("#thumbnail").html();
		$("#name-thumbnail").val(pre);
		
		var pre = $("#frame-prev").contents().find("#prev").html();
		$("#name-prev").val(pre);
		
		var ori = $("#frame-ori").contents().find("#ori").html();		
		nameOri = $("#name-ori").val(ori);
		$("#frame-ori").contents().find("#fuprev").submit();
	
			
		var n=ori.split(".");
		//alert(n[2]);
		var arr_length = n.length;
		$("#ext-ori").val(n[arr_length-1]);
		//return false;		
	});		
	
	/* example for autocomplete jquery token input
	$("#demo-input-facebook-theme").tokenInput("http://shell.loopj.com/tokeninput/tvshows.php", {
		theme: "facebook"
	});
	$("input[type=button]").click(function () {
            alert("Would submit: " + $(this).siblings("input[type=text]").val());
    });
	*/
	$("#private_is").click(function(){
		if ($(this).is(":checked")){
			$("#is_private").val("1");
			$("#private").show();
		}
		else{
			$("#is_private").val("0");
			$("#private").hide();
		}	
	});
	
	$("#share_user").tokenInput("<?php echo base_url()?>user/get", {
		theme: "facebook",
		preventDuplicates: true
	});
	
	$("#share_user").on("change",function(){
		$("#user_private").val($(this).val());
	});
</script>