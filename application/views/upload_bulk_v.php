<link rel="stylesheet" href="<?php echo asset_url()?>plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo asset_url()?>plupload/js/plupload.full.min.js"></script>
<script type="text/javascript" src="<?php echo asset_url()?>plupload/js/jquery.plupload.queue/jquery.plupload.queue.js"></script>

<!-- debug 
<script type="text/javascript" src="../../js/moxie.js"></script>
<script type="text/javascript" src="../../js/plupload.dev.js"></script>
<script type="text/javascript" src="../../js/jquery.ui.plupload/jquery.ui.plupload.js"></script>
-->
<?php $this->load->view('upload_v');?><br/>
<div style="display:none" id="add">1</div>
<form method="post" action="<?php echo base_url()?>upload/post">	
	<?php $this->load->view('group_cat_sub_sel'); ?>
	<br>
	<div id="uploader">
		<p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
	</div>
	<input class="btn" type="submit" value="Send" />
</form>
<script type="text/javascript">
	group();
$(function() {
	
	// Setup html5 version
	$("#uploader").pluploadQueue({
		// General settings
		runtimes : 'html5,flash,silverlight,html4',
		url : '<?php echo base_url()?>upload/proccess',
		chunk_size: '1mb',
		rename : true,
		dragdrop: true,
		
		filters : {
			// Maximum file size
			max_file_size : '2000mb',
			// Specify what files to browse for
			mime_types: [
				{
					title : "Allowed files", 
					extensions : "mp3,wma,wav,ogg,jpg,png,gif,ai,psd,cdr,bmp,flv,mp4,avi,mpg,zip,rar,doc,docx,xls,xlsx,ppt,pptx,pdf"
				}
			]
		},

		// Resize images on clientside if we can
		//resize : {width : 320, height : 240, quality : 90},

		flash_swf_url : '../../js/Moxie.swf',
		silverlight_xap_url : '../../js/Moxie.xap'
	});

});
</script>
