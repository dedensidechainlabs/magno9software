<style>	

	

.button-green
{
    font-family: "Segoe UI","Segoe","Verdana";
    background: #0f9e0a center top no-repeat;
    background-image: linear-gradient(rgb(15, 158, 10), rgb(24, 105, 21)); 
    overflow: hidden;
    color: white;
    border-radius: 5px;
    width: 82px;  
    position: relative; 
    padding: 8px 10px 8px 10px;
}

.button-green:hover
{
	cursor:pointer;
    background: #0a8406 center top no-repeat;
    background-image: linear-gradient(rgb(12, 141, 8), rgb(19, 88, 17));     
}

.file-upload
{
    opacity: 0;
    width: 102px;
    height: 35px;
    position: absolute;
    top: 0px;
    left: 0px;
}

.file-upload:hover
{
	cursor:pointer;
}
.error{
	font-family: Arial;
	font-size:14px;
	float:right;
	color:red;
	width:40%;
}
</style>	
<link href="<?php echo asset_url()?>bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<script src="<?php echo asset_url()?>js/jquery-1.9.1.js" type="text/javascript"></script>
<div id="fr">
<div class="error"><?php  echo $error;?></div>
<form id="fuprev" action="<?php echo base_url().'site/upload_ori_post'?>" method="post" enctype="multipart/form-data">
<div class="button-green">
	<input type="file" class="file-upload" name="userfile"  required/>
	Upload File
</div>

<input style="display:none" type="submit" class="btn" id="sub" value="upload" />
</form>
</div>
<div style="display:none" id="wait">Upload in Progress...</div>


<script>
$('input[type=file]').change(function() { 
    // select the form and submit
    $('form').submit();
});

$('form').bind('submit', function() {
	$("#fr").fadeOut(50,function(){
		$('#wait').fadeIn(10);
	});
});	

</script>