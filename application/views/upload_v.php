<?php 
	if ($this->uri->segment(2)=='bulk')
	{
		$bulk = 'btn-primary';
		$single = '';
	}
	else if ($this->uri->segment(2)=='add')
	{	
		$bulk = '';
		$single = 'btn-primary';
	}
	else
	{
		$bulk = '';
		$single = '';
	}
?>
<center>
	<a class="btn <?php echo $bulk; ?>" href="<?php echo site_url("upload/bulk")?>">Multiple Upload</a>
	<a class="btn <?php echo $single; ?>" href="<?php echo site_url("file/add")?>">Single Upload</a>
</center>