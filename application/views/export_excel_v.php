<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MAGNO9 - Digital Asset Management System </title> 
	<meta name="description" content="DAMS">
	<meta name="keywords" content="DAMS">
	<link href="<?php echo asset_url()?>css/report.css" rel="stylesheet" type="text/css" />   
</head>
<body>
	<h1> Magno9 Report </h1>
	Group: <b><?php echo $group_name; ?></b><br>
	Category: <b><?php echo $cat_name?></b><br>
	Sub Category: <b><?php echo  $sub_name?></b><br>
	Year: <b><?php echo  $year?></b><br><br>
	
	
    <table>
		<colgroup>
			<col style="width:65px;">
			<col style="">
			<col style="">
			<col style="text-align:center;">
			<col style="text-align:center;">
		</colgroup>		
		<tr>
			<th>No</th>
			<th>Group</th>
			<th >Category</th>
			<th >Sub Category</th>
			<th >Title</th>
			<th >Code</th>
			<th >Description</th>
			<th >Keyword</th>
			<th >Extension</th>
			<th >Date</th>
			
		</tr>
		<?php
		$no = 0;
		foreach($query as $row)
		{	
			$no++?>	
			<tr>
				<td><?php echo $no ?></td>
				<td><?php echo $this->S->get_group(array('id'=>$row->file_group_id))->row()->group_name; ?></td>
				<td><?php echo $this->S->get_category(array('id'=>$row->file_cat_id))->row()->cat_name; ?></td>
				<td><?php echo $this->S->get_subcategory(array('id'=>$row->file_sub_id))->row()->sub_name; ?></td>				
				<td><?php echo $row->file_title; ?></td>
				<td><?php echo get_code($row->file_id) ; ?></td>
				<td><?php echo $row->file_desc; ?></td>
				<td><?php echo $row->file_keyword; ?></td>
				<td><?php echo $row->file_extension; ?></td>
				<td><?php echo date_indo_notime($row->file_created_date);?></td>
			</tr>
			<?php
		}
		?>
	</table>
</body>
</html>
