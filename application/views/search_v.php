<form id="fsearch" action="<?php echo site_url("search");?>" method="get">
	<div class="merah">
				<select name="group_id" id="group">
					<option value="">-- All Group --</option>
					<?php 
					$par = array('view'=>1);
					$qbla = get_group($par); 					
					?>
					<?php 
					foreach ($qbla as $row)
					{
						if ($this->input->get('group_id') == $row->group_id)
							{$sel = "selected";}
						else
							{$sel = "";}						
						echo "<option value='$row->group_id' $sel>$row->group_name</option>";
					}
					?>
				</select>
	</div>
			<div class="merah">
				<select name="media" id="media">
					<option  value="">-- All File Type --</option>
					<option value="audio" <?php if($this->input->get('media')=='audio'){echo "selected";}?>  >Audio</option>
					<option value="compress" <?php if($this->input->get('media')=='compress'){echo "selected";}?> >Compress</option>
					<option value="document" <?php if($this->input->get('media')=='document'){echo "selected";}?> >Document</option>
					<option value="image" <?php if($this->input->get('media')=='image'){echo "selected";}?> >Image</option>
					<option value="video" <?php if($this->input->get('media')=='video'){echo "selected";}?>>Video</option>
				</select>
			</div>
			<div class="merah">
				<select name="year" id="year">
					<option value="">-- All Year --</option><?php
					$qyear_sel = get_file_yearlist();				
					foreach ($qyear_sel as $rw)
					{
						if ($rw->thn == $this->input->get('year'))
						{
							echo "<option value='$rw->thn' selected>$rw->thn</option>";				
						}
						else
						{
							echo "<option value='$rw->thn'>$rw->thn</option>";
						}
					}
					?>
				</select>
			</div>
			<input placeholder="SEARCH" list="title" name="find" value="<?php echo (isset($find)?$find:"")?>" >
			<datalist id="title">
			<?php
				$p = array(
					'act'	=> 1,
					'limit'	=> 10
				);
				$q = $this->S->get_file($p)->result();				
				foreach($q as $row)
				{	?>
					<option value="<?php echo $row->file_title?>">
					<?php
				}
			?>
			</datalist><br/><br/>
			<input class="btn btn-primary" type="submit" name="submit" value="GO">
			</form> 
			<!--<div style="cursor:pointer;" id="go"><img src="<?php echo image_url()?>go.png"></div>-->
			
			<br><br>
			Disk Space: <br>
			<?php
    
	
	$size_byte = dirsize('files/');
	$size_KB = $size_byte/1024; 
	$size_MB = $size_KB/1024; 	
	$storage_MB  = get_Initial()->max_storage*1024;
	$space_MB =  number_format($storage_MB - $size_MB,0,",",".");
	$max_storage = number_format($storage_MB,0,",",".");
	echo "<h2>$space_MB MB free of $max_storage MB</h2>";	
	?>