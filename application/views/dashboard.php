<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MAGNO9 - Digital Asset Management System </title> 
	<meta name="description" content="DAMS">
	<meta name="keywords" content="DAMS">

	<link href="<?php echo image_url()?>dams_icon.ico" rel="icon" type="image/x-icon" />
	<link href="<?php echo asset_url()?>css/template1.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo asset_url()?>jquery-ui/css/ui-lightness/jquery-ui-1.10.3.custom.css"  rel="stylesheet" type="text/css" />
	<script src="<?php echo asset_url()?>js/jquery-2.0.2.js" type="text/javascript"></script>
	<script src="<?php echo asset_url()?>jquery-validation-1.11.1/jquery.validate.js" type="text/javascript"></script>
	<script src="<?php echo asset_url()?>js/jquery-ui.js" type="text/javascript"></script>
	<script src="<?php echo asset_url();?>fancyBox/jquery.fancybox.js" type="text/javascript"></script>
	<script src="<?php echo asset_url()?>js/modernizr.js"></script>
	 <script type="text/javascript" src="<?php echo asset_url()?>jquery-multiple-token/jquery.tokeninput.js"></script>
    <link rel="stylesheet" href="<?php echo asset_url()?>jquery-multiple-token/token-input-facebook.css" type="text/css" />
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
</head>
	
<body>
<?php $this->load->view('header')?>

<?php  //echo "<pre>";print_r($this->session->userdata);echo "</pre>"; ?>
<div class="container">
	<div class="right-container">
		<div class="conair">
			<div class="bc-red"></div>
			<div class="bc"><?php $this->load->view('bc_v'); ?>	</div> 
			<div class="content">
				<?php 
				if (isset($content) && $content!='')
					{$this->load->view($content);}
				else
					{$this->load->view('admin_v');}				
				?>
				<div class="clr"></div>
			</div>
		</div>
	</div>
	<div class="left-container">
		<div class="menu">
			<?php //echo "<pre>";print_r(get_smtp());echo "</pre>"; $u = get_smtp();?>
			<?php $this->load->view('menu');?>
		</div>
		<div class="search-container">
			<?php $this->load->view('search_v');?>
		</div>
	</div>
	<div class="clr"></div>
	
</div>	
<?php $this->load->view('footer')?>
</body>
</html>
<?php $this->load->view('magno_js');?>