
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>MAGNO9 - Digital Asset Management System </title> 
	<meta name="description" content="DAMS">
	<meta name="keywords" content="DAMS">

	<link href="<?php echo image_url()?>dams_icon.ico" rel="icon" type="image/x-icon" />
	<link href="<?php echo asset_url()?>css/template1.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo asset_url()?>js/jquery-2.0.2.js" type="text/javascript"></script>
	<script src="<?php echo asset_url()?>jquery-validation-1.11.1/jquery.validate.js" type="text/javascript"></script>
	<script src="<?php echo asset_url()?>js/jquery-ui.js" type="text/javascript"></script>
	<script src="<?php echo asset_url()?>js/modernizr.js"></script>
	<style>
body { 
	margin:0;
	padding:0;
	background:none; 
}
</style>
</head>
<body>
<div class="add-edit-cont">
	<form id="fcpass" action="<?php echo base_url()?>password/post" method="post">	
	<table>
		<colgroup>
			<col style="width:20%">
			<col style="width:80%">
		</colgroup>
		<tr>
			<td>User Name</td>
			<td><input type="text" maxlength="20" name="user_name" id="user_name" value="<?php echo $this->jCfg['user']['name']?>" disabled></td>
		</tr>
		<tr>
			<td>Old password</td>
			<td><input type="password" name="old_password" maxlength="10"  value=""></td>
		</tr>
		<tr>
			<td>New password</td>
			<td><input type="password" id="new_password" name="new_password" maxlength="10"  value=""></td>
		</tr>
		<tr>
			<td>Confirm New password</td>
			<td><input type="password" name="new_password_conf" maxlength="10"  value=""></td>
		</tr>
		<tr>
			<td colspan="2">
				<div class="error"><?php echo (isset($error)?$error:"")?></div>
				<div class="success"><?php echo (isset($success)?$success:"")?></div>
			</td>
		</tr>
		<tr>
			<td style="border:1 px solid #cbcbcb;" colspan="2">
			<input name="submit" id="save" type="submit" value="Submit" class="btn btn-primary"></td>
		</tr>
	</table>
	</form>
</div>		
</body>
</html>
<script>
	$("#fcpass").validate({
		rules: {
			new_password: {
				minlength: 3
			},
			new_password_conf:{
				equalTo: "#new_password"
			}
		}
	});
</script>