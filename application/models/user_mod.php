<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User_mod extends CI_Model 
{
	function __construct()
    {
        parent::__construct();
		$this->table = 'dam_user';
    }
	
	function get($p=array())
	{		
		if (isset($p['id']) && $p['id'])
			{$this->db->where('user_id',$p['id']);}
		if (isset($p['name']) && $p['name'])
			{$this->db->where('user_name',$p['name']);}
		if (isset($p['level']) && $p['level'])
			{$this->db->where('level',$p['level']);}
		if (isset($p['password']) && $p['password'])
			{$this->db->where('password',sha1($p['password']));}
		if (isset($p['active']) && $p['active']==1)
			{$this->db->where('active',1);}
		if (isset($p['fullname']) && $p['fullname'])
			{$this->db->where('fullname',$p['fullname']);}
		if( isset($p['by']) && isset($p['order']))	
			{$this->db->order_by($p['by'],$p['order']);}
		else
			{$this->db->order_by('user_id','desc');}
		if (isset($p['limit']) && $p['limit']!='')
			{$this->db->limit($p['limit']);}			
		$result = $this->db->get($this->table);
		return $result;	
	}
	
	function cek($p=array())
	{
		$this->db->where('user_name',$p['user_name']);	
		$this->db->where('password',sha1($p['password']));
		$result = $this->db->get($this->table);
		return $result;	
	}
	
	/*
	function user_add($p=array())
	{
		$p = array(	
			'name' 			=>  $p['name'],
			'email' 		=>	$p['email'],
			'created_date'	=>  date("Y-m-d H:i:s"),
			'created_ip'	=>  $this->input->ip_address()			
		);
		return $this->db->insert('user',$p);
	}
	*/
}	
	