<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Site_mod extends CI_Model 
{
	function __construct()
    {
        parent::__construct();
		//$this->db = $this->load->database('default', TRUE);  
    }
	
	function get_group($p=array())
	{		
		if (isset($p['id']) && $p['id'])
			{$this->db->where('group_id',$p['id']);}
		if (isset($p['name']) && $p['name'])
			{$this->db->where('group_name',$p['name']);}
		if (isset($p['act']) && $p['act']==1)
			{$this->db->where('active',1);}
		if( isset($p['by']) && isset($p['order']))	
			{$this->db->order_by($p['by'],$p['order']);}
		else
			{$this->db->order_by('dam_group.group_id','desc');}
		if (isset($p['access']) && $p['access'])	
		{
			$this->db->join('dam_group_access','dam_group_access.group_id=dam_group.group_id');
			$this->db->where('dam_group_access.user_id',$p['access']['user']);
			if (isset($p['access']['add'])&& $p['access']['add']==1)
				{$this->db->where('add',1);}
			if (isset($p['access']['view'])&& $p['access']['view']==1)
				{$this->db->where('view',1);}
			if (isset($p['access']['down'])&& $p['access']['down']==1)
				{$this->db->where('download',1);}
			if (isset($p['access']['edit'])&& $p['access']['edit']==1)
				{$this->db->where('edit',1);}
			if (isset($p['access']['del'])&& $p['access']['del']==1)
				{$this->db->where('delete',1);}
		}
		if (isset($p['limit']) && $p['limit']!='')
			{$this->db->limit($p['limit']);}			
		$result = $this->db->get('dam_group');
		return $result;	
	}
	
	function get_group_access($p=array())
	{				
		if (isset($p['user_id']) && $p['user_id'])
			{$this->db->where('user_id',$p['user_id']);}
		if (isset($p['group_id']) && $p['group_id'])
			{$this->db->where('group_id',$p['group_id']);}
		$result = $this->db->get('dam_group_access');
		return $result;
	}
	
	public function cek_group_access($user_id,$grid)
	{
		$this->db->where('user_id',$user_id);
		$this->db->where('group_id',$grid);
		return $this->db->get('dam_group_access')->row();	
	}
	
	public function group_file_access($p=array())
	{
		if (isset($p['file_id']) && $p['file_id'])
		{
			$this->db->where('file_id',$p['file_id']);
			$this->db->join('dam_file','file_group_id==group_id');
		}
		if (isset($p['user_id']) && $p['user_id'])
			{$this->db->where('user_id',$p['user_id']);}
		if (isset($p['group_id']) && $p['group_id'])
			{$this->db->where('group_id',$p['group_id']);}
		return $this->db->get('dam_group_access')->row();	
	}
	
	function get_category($p=array())
	{		
		if (isset($p['id']) && $p['id'])
			{$this->db->where('cat_id',$p['id']);}
		if (isset($p['group_id']) && $p['group_id'])
			{$this->db->where('cat_group_id',$p['group_id']);}
		if (isset($p['name']) && $p['name'])
			{$this->db->where('cat_name',$p['name']);}
		if (isset($p['act']) && $p['act']==1)
			{$this->db->where('active',1);}
		if( isset($p['by']) && isset($p['order']))	
			{$this->db->order_by($p['by'],$p['order']);}
		else
			{$this->db->order_by('cat_id','desc');}
		if (isset($p['limit']) && $p['limit']!='')
			{$this->db->limit($p['limit']);}			
		$result = $this->db->get('dam_category');
		return $result;	
	}
	
	function get_category_id($id)
	{
		$this->db->where('cat_id',$id);
		//$this->db->group_by('cat_group_id');
		$result = $this->db->get('dam_category');
		return $result;
	}
	
	function get_subcategory($p=array())
	{		
		if (isset($p['id']) && $p['id'])
			{$this->db->where('sub_id',$p['id']);}
		if (isset($p['cat_id']) && $p['cat_id'])
			{$this->db->where('sub_cat_id',$p['cat_id']);}
		if (isset($p['name']) && $p['name'])
			{$this->db->where('sub_name',$p['name']);}
		if (isset($p['act']) && $p['act']==1)
			{$this->db->where('active',1);}
		if( isset($p['by']) && isset($p['order']))	
			{$this->db->order_by($p['by'],$p['order']);}
		else
			{$this->db->order_by('sub_id','desc');}
		if (isset($p['limit']) && $p['limit']!='')
			{$this->db->limit($p['limit']);}			
		$result = $this->db->get('dam_subcategory');
		return $result;	
	}
	
	function get_file($p=array())
	{				
		if (isset($p['more']) && $p['more'] != '')
			{$this->db->where('file_id <',$p['more']);}
		if (isset($p['id']) && $p['id'])
			{$this->db->where('file_id',$p['id']);}
		if (isset($p['name']) && $p['name'])
			{$this->db->where('file_title',$p['name']);}
		//where sub_id, cat_id, group_id
		if (isset($p['sub_id']) && $p['sub_id'])
			{$this->db->where('file_sub_id',$p['sub_id']);}		
		if (isset($p['cat_id']) && $p['cat_id'])
			{$this->db->where('file_cat_id',$p['cat_id']);}
		if (isset($p['group_id']) && $p['group_id'])
			{$this->db->where('file_group_id',$p['group_id']);}				
		//file created_date
		if (isset($p['file_date']) && $p['file_date'])
			{$this->db->where('file_created_date',$p['file_date']);}
		if (isset($p['ext']) && $p['ext']!='')
			{$this->db->where('file_extension',$p['ext']);}
		
		if (isset($p['act']) && $p['act']==1)
			{$this->db->where('dam_file.active',1);}
		if( isset($p['by']) && isset($p['order']))	
			{$this->db->order_by($p['by'],$p['order']);}
		else
			{$this->db->order_by('dam_file.file_id','desc');}
		if (isset($p['limit']) && $p['limit']!='')
			{$this->db->limit($p['limit']);}
		
		if (isset($p['find']) && $p['find'])
		{
			$this->db->where("(
    			file_title LIKE '%".$p['find']."%'
    			OR file_desc LIKE '%".$p['find']."%'
    			OR file_keyword LIKE '%".$p['find']."%'
				OR file_code LIKE '%".$p['find']."%'
    		)");
		}
		if (isset($p['year']) && $p['year'])
			{$this->db->where('MID(file_created_date,1,4)',$p['year']);}
		if (isset($p['type']) && $p['type'])
		{
			$this->db->join('file_extension','file_extension.file_ext_name=dam_file.file_extension');
			$this->db->join('dam_filetype','file_extension.file_type=dam_filetype.filetype_name');
			$this->db->where('filetype_name',$p['type']);
		}
		
		/*if (isset($p['is_private']) && $p['is_private']==1)
		{	
			$this->db->or_where('is_private',1);
			if (isset($p['private']) && $p['private'])
			{			
				$this->db->join('dam_file_access','dam_file_access.file_id=dam_file.file_id','left');
				$this->db->where('dam_file_access.user_id',$p['private']['user']);
				//$this->db->where('dam_file_access.file_id',$p['private']['file']);
				//$this->db->or_where('is_private',1);			
				if (isset($p['private']['view'])&& $p['private']['view']==1)
					{$this->db->where('dam_file_access.view_download',1);}
				//if (isset($p['private']['down'])&& $p['private']['down']==1)
					//{$this->db->where('dam_file_access.download',1);}			
			}
		}*/
		
		//$this->db->where('is_private',0);		
		
		if (isset($p['access']) && $p['access'])
		{			
			$this->db->join('dam_group_access','file_group_id=group_id');
			if (isset($p['access']['user']) && $p['access']['user'])
				{$this->db->where('dam_group_access.user_id',$p['access']['user']);}
			if (isset($p['access']['group']) && $p['access']['group'])
				{$this->db->where('dam_group_access.group_id',$p['access']['group']);}
			if (isset($p['access']['view'])&& $p['access']['view']==1)
				{$this->db->where('dam_group_access.view',1);}
			if (isset($p['access']['down'])&& $p['access']['down']==1)
				{$this->db->where('dam_group_access.download',1);}
			if (isset($p['access']['edit'])&& $p['access']['edit']==1)
				{$this->db->where('dam_group_access.edit',1);}
			if (isset($p['access']['del'])&& $p['access']['del']==1)
				{$this->db->where('dam_group_access.delete',1);}
		}
		if (isset($p['offset']) && isset($p['limit_get'])  && $p['limit_get'] !='')
		{
			$result = $this->db->get('dam_file',$p['limit_get'],$p['offset']);
		}
		else
		{
			$result = $this->db->get('dam_file');
		}
		return $result;	
	}	
	
	function check_file_access($p=array())
	{		
		$this->db->where('user_id',$p['user_id']);	
		$this->db->where('file_id',$p['file_id']);
		$result = $this->db->get('dam_file_access');
		return $result;
	}
	
	// allowed edit files by created_by 
	function edit_file_access($p=array())
	{		
		$this->db->where('created_by',$p['user_id']);	
		$this->db->where('file_id',$p['file_id']);
		$result = $this->db->get('dam_file');
		return $result;
	}
	
	function get_file_access($p=array())
	{		
		if (isset($p['user_id']) && $p['user_id'])
			{$this->db->where('user_id',$p['user_id']);}
		if (isset($p['file_id']) && $p['file_id'])
			{$this->db->where('file_id',$p['file_id']);}
		$result = $this->db->get('dam_file_access');
		return $result;
	}
	
	function get_icon($p=array())
	{		
		if (isset($p['sel']) && $p['sel']==1) //selected icon id and icon image
			{$this->db->select('icon_id,icon_image');}
		if (isset($p['id']) && $p['id'])
			{$this->db->where('icon_id',$p['id']);}
		if (isset($p['act']) && $p['act']==1)
			{$this->db->where('icon_active',1);}
		if( isset($p['by']) && isset($p['order']))	
			{$this->db->order_by($p['by'],$p['order']);}
		else
			{$this->db->order_by('icon_id','desc');}
		$result = $this->db->get('dam_icon');
		return $result;
	}
	
	function get_extension($p=array())
	{		
		if (isset($p['ext']) && $p['ext'])
			{$this->db->where('file_ext_name',$p['ext']);}	
		$result = $this->db->get('file_extension');
		return $result;
	}
	
	function get_file_type($p=array())
	{		
		if (isset($p['type']) && $p['type'])
			{$this->db->where('filetype_name',$p['type']);}	
		$result = $this->db->get('dam_filetype');
		return $result;
	}
	
	function get_file_yearlist()
	{
		//using active record but shit!! error guys
		/*$this->db->select('MID(file_created_date,1,4) as thn');
		$this->db->group_by('MID(file_created_date,1,4)');
		return $this->db->get('dam_file');*/
		
		return $this->db->query("SELECT MID(file_created_date,1,4) AS thn	FROM dam_file GROUP BY thn");
	}
	
}	
	