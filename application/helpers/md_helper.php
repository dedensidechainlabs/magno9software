<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function file_url(){
	$CI =& get_instance();
	return $CI->config->slash_item('file_url');
}

function image_url(){
	$CI =& get_instance();
	return $CI->config->slash_item('image_url');
}

function asset_url(){
	$CI =& get_instance();
	return $CI->config->slash_item('asset_url');
}

function theme_url(){
	$CI =& get_instance();
	return $CI->config->slash_item('theme_url');
}

function date_indo($value) 
{
	if (substr($value,0,10)== date("Y-m-d"))
		{return date("H:i",strtotime($value)); }
	else
		{return date("d M y",strtotime($value));} 
}

function date_indo_notime($value) 
{
	return date("d M y",strtotime($value));
}

function get_group(){
	$CI =& get_instance();
    // You may need to load the model if it hasn't been pre-loaded
    $CI->load->model('site_mod');
    // Call a function of the model
	if ($CI->session->userdata['jcfg']['user']['level']=='administrator')
		{$access = 0;}
	else
	{
		$access = array(
			'user'	=> $CI->session->userdata['jcfg']['user']['id'],
			'view'	=> 1
		);
	}
	$p = array(
		'act'	=> 1,
		'order' => 'asc',
		'by'	=> 'group_name',
		'access' => $access 
	);	
    $q = $CI->site_mod->get_group($p)->result();		
	return $q;
}

function get_category(){
	$CI =& get_instance();
    // You may need to load the model if it hasn't been pre-loaded
    $CI->load->model('site_mod');
    // Call a function of the model
	$p = array(
		'act'	=> 1,
		'by'	=> 'cat_name',
		'order'	=> 'asc'
	);
    $q = $CI->site_mod->get_category($p)->result();		
	return $q;
}

function get_subcategory(){
	$CI =& get_instance();
    // You may need to load the model if it hasn't been pre-loaded
    $CI->load->model('site_mod');
    // Call a function of the model
	$p = array(
		'act'	=> 1,
		'by'	=> 'sub_name',
		'order'	=> 'asc'
	);
    $q = $CI->site_mod->get_subcategory($p)->result();		
	return $q;
}



function get_file_yearlist(){
	$CI =& get_instance();
    // You may need to load the model if it hasn't been pre-loaded
    $CI->load->model('site_mod');
    // Call a function of the model
    $q = $CI->site_mod->get_file_yearlist()->result();		
	/*$thn = '';
	foreach ($q as $row)
	{
		$thn .= $row->thn;
	}*/
	return $q;
}

function extension_allowed($par)
{	
	$CI =& get_instance();
    // You may need to load the model if it hasn't been pre-loaded
    $CI->load->model('site_mod');	
	$q = $CI->site_mod->get_extension(array('ext'=>$par))->result();	
	return $q;
}

function get_Initial()
{		
	$CI =& get_instance();
	$result = $CI->db->get('dam_initial')->row();
	return $result;
}

function get_smtp()
{
	$CI =& get_instance();
	$smtp = array(
		'host'		=> rtrim($CI->config->slash_item('smtp_host'),"/"),
		'username'	=> rtrim($CI->config->slash_item('smtp_username'),"/"),
		'password'	=> rtrim($CI->config->slash_item('smtp_password'),"/")
	);
	return $smtp;
}

function delete($p=array())	
{
	$CI =& get_instance();
	$CI->db->where($p['pk'],$p['value']);
	$result = $CI->db->delete($p['table_name']);
	return $result;
}

function icon_img($id)	
{
	$CI =& get_instance();
	$CI->load->model('site_mod');	
	return $CI->site_mod->get_icon(array('id' => $id))->row()->icon_image;
}

function get_code($id)	
{
	$CI =& get_instance();
	$CI->load->model('site_mod');	
	$q = $CI->site_mod->get_file(array('id' => $id))->row();
	if (count($q)==1)
	{
		$fcd = $q->file_created_date;
		$str_fcd = explode("-",$fcd);
		$prefix = $CI->site_mod->get_group(array('id' => $q->file_group_id))->row()->group_prefix;
		$code = $prefix.'-'.$str_fcd[0].$str_fcd[1].$str_fcd[2].'-'.$q->file_id; 
		return $code;
	}
	else
	{
		return FALSE;
	}
}

function dirsize($dir)
{
      @$dh = opendir($dir);
      $size = 0;
      while ($file = @readdir($dh))
      {
        if ($file != "." and $file != "..") 
        {
          $path = $dir."/".$file;
          if (is_dir($path))
          {
            $size += dirsize($path); // recursive in sub-folders
          }
          elseif (is_file($path))
          {
            $size += filesize($path); // add file
          }
        }
      }
      @closedir($dh);
      return $size;
}

function ceksize()
{
	if ((dirsize('files/')/1024)/1024 >= get_Initial()->max_storage*1024)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
?>